#include "SDLEventsHandler.h"

SDLEventsHandler::SDLEventsHandler() : m_windowsClosedEvent(false), m_click(false),m_wheelmov(0)
{

}

SDLEventsHandler::~SDLEventsHandler()
{

}

void SDLEventsHandler::updateInputs()
{
    bool wheeled = false;
    //getting all the pending keyboard events
    while (SDL_PollEvent(&m_currentEvent))
    {
        if(m_currentEvent.type == SDL_EventType::SDL_KEYDOWN)
        {
            m_keyboardPressMap[m_currentEvent.key.keysym.sym] = true; 
        }
        else if(m_currentEvent.type == SDL_EventType::SDL_KEYUP)
        {
            m_keyboardPressMap[m_currentEvent.key.keysym.sym] = false; 
        }
        else if(m_currentEvent.type == SDL_EventType::SDL_QUIT)
        {
            m_windowsClosedEvent = true;
        }
        else if(m_currentEvent.type == SDL_EventType::SDL_MOUSEBUTTONDOWN && m_currentEvent.button.button == SDL_BUTTON_LEFT)
        {
            m_click = true;
            m_clickCoordinates.x =  m_currentEvent.button.x;
            m_clickCoordinates.y =  m_currentEvent.button.y;
        }
        else if(m_currentEvent.type == SDL_EventType::SDL_MOUSEBUTTONUP && m_currentEvent.button.button == SDL_BUTTON_LEFT)
        {
            m_click = false;
        }
        else if(m_currentEvent.type == SDL_MOUSEWHEEL)
        {
            m_wheelmov = m_currentEvent.wheel.y;
            wheeled = true;
        }
    }
    if(!wheeled) m_wheelmov = 0;
}

bool SDLEventsHandler::isPressed(SDL_Keycode code)
{
    auto it = m_keyboardPressMap.find(code);
    if (it == m_keyboardPressMap.end())
    {
        return false;
    }
    return it->second;
}

bool SDLEventsHandler::isWindowsClosed()
{
    return m_windowsClosedEvent;
}

bool SDLEventsHandler::isClick(vec2& coor)
{
    coor = m_clickCoordinates;
    return m_click;
}

bool SDLEventsHandler::wheelMoved(int32_t& mov)
{
    if( m_wheelmov == 0) return false;
    mov = m_wheelmov;
    return true;
}