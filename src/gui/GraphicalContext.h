#ifndef GRAPHICALCONTEXT_H
#define GRAPHICALCONTEXT_H

#include "common.h"
#include "gui/SDLEventsHandler.h"
#include "simulation/Tracks.h"
#include "simulation/Car.h"
#include "simulation/Pedestrian.h"
#include "simulation/Simulation.h"
#include "simulation/SimulationBank.h"

class GraphicalContext
{
    public:
        GraphicalContext(SimulationBank*,size_t savesSpace,Simulation*);
        ~GraphicalContext();

        //main display loop, shouldn't be called outside of the graphical thread
        void run(float dt);

        void setCenter(vec2);

    private:
        //Main window
        SDL_Window* m_window;
        //OPENGL context
        SDL_GLContext m_openglContext;
        //Renderer is required for all sdl display functions
        SDL_Renderer* m_screenRenderer;

        //The tracks, for display purposes
        Tracks* m_tracks;
        vec2* m_wallPoints;
        vec2* m_beginWall;
        vec2* m_endWall;
        vec2* m_beginCap;
        size_t m_wallArraysLength;
        void updateTracks();
        void updateCars();
        float m_wallThickness;

        //The cars
        std::vector<Car*> m_cars;
        size_t m_carsLength;
        size_t m_focusedCarIndex;

        //The pedestrians
        std::vector<Pedestrian*> m_pedestrians;
        size_t m_pedestriansLength;

        //the display options
        float m_viewScale;
        vec2 m_viewCenter;
        float m_viewRatio;
        void displayAll();
        void displayPedestrians();
        void displayCars();
        void displayWalls();
        void displaySensors();
        void displayNN();
        void displayGraph();
        void glVertexWrapperToScreen(vec2);

        //event handling
        //This is used to collect and lookup all events
        SDLEventsHandler m_eventHandler;
        bool handleInput();
        bool m_rpush;
        bool m_nbpush;
        bool m_upPush;
        bool m_downPush;
        bool m_ppush;
        bool m_rightPush;
        bool m_leftPush;
        bool m_clicking;
        bool m_hpush;

        //the simulation
        Simulation* m_simulation;
        SimulationBank* m_simulationStack;
        size_t m_saveStackIndex;
        size_t m_saveSpace;
        void simulateStep();
        bool m_paused;
        bool m_forward;
        
        //the graph
        std::vector<double> m_maxFitnessLog;
        std::vector<double> m_avgFitnessLog;
        vec2* m_maxGraphPoints; //101 points
        vec2* m_avgGraphPoints; //101 points

};

#endif