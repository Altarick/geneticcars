#include "gui/GraphicalContext.h"

#include <neural_network/neural_synapse.h>
#include <neural_network/neuron.h>

const int WIDTH = 1200;
const int HEIGHT = 800;

GraphicalContext::GraphicalContext(
    SimulationBank* simulBank,
    size_t savesSpace,
    Simulation* simul) : 
        m_window(nullptr),
        m_screenRenderer(nullptr),
        m_tracks((*simulBank)[0]->getTracks()),
        m_wallPoints(nullptr),
        m_beginWall(nullptr),
        m_endWall(nullptr),
        m_beginCap(new vec2[12]),
        m_wallArraysLength(0),
        m_wallThickness(1),
        m_cars((*simulBank)[0]->getCars()),
        m_carsLength(m_cars.size()),
        m_focusedCarIndex(m_carsLength == 0 ? -1 : 0),
        m_pedestrians((*simulBank)[0]->getPedestrians()),
        m_pedestriansLength(m_pedestrians.size()),
        m_viewScale(0.01),
        m_viewCenter(0, 0),
        m_viewRatio(((float)WIDTH)/((float)HEIGHT)),
        m_rpush(false),
        m_nbpush(false),
        m_upPush(false),
        m_downPush(false),
        m_ppush(false),
        m_rightPush(false),
        m_leftPush(false),
        m_clicking(false),
        m_hpush(false),
        m_simulation(simul),
        m_simulationStack(simulBank),
        m_saveStackIndex(0),
        m_saveSpace(savesSpace),
        m_paused(false),
        m_forward(true),
        m_maxGraphPoints(new vec2[101]),
        m_avgGraphPoints(new vec2[101])
{
    //initialisations for SDL
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        std::cout << "Error initialising SDL :" << SDL_GetError() << std::endl;
        exit(-1);
    }
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    //Window creation
    if (nullptr == (m_window = SDL_CreateWindow("GeneticCars !", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN)))
    {
        std::cout << "Error creating window :" << SDL_GetError() << std::endl;
        exit(-2);
    }
    //creating opengl context
    if (nullptr == (m_openglContext = SDL_GL_CreateContext(m_window)))
    {
        std::cout << "Error while trying to create a Opengl 3.3 context :" << SDL_GetError() << std::endl;
        exit(-3);
    }
    //getting renderer
    if (nullptr == (m_screenRenderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED)))
    {
        std::cout << "Error while trying to get a renderer :" << SDL_GetError() << std::endl;
        exit(-4);
    }
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    GLfloat mat[16] = { 
                        1,0,0,0,
                        0,m_viewRatio,0,0,
                        0,0,1,0,
                        0,0,0,1
                    };
    glLoadMatrixf(mat);

    glClearColor(1.f, 1.f, 1.f, 1.f);

    //getting track display points
    m_beginWall = new vec2[4];
    m_endWall = new vec2[4];



    updateTracks();
}

GraphicalContext::~GraphicalContext()
{
    delete[] m_maxGraphPoints;
    delete[] m_avgGraphPoints;
    if (m_wallPoints != nullptr)
        delete[] m_wallPoints;
    delete[] m_beginWall;
    delete[] m_endWall;
    delete[] m_beginCap;
    SDL_DestroyRenderer(m_screenRenderer);
    m_screenRenderer = nullptr;
    SDL_DestroyWindow(m_window);
    m_window = nullptr;
    //ending the SDL
    SDL_Quit();
}

void GraphicalContext::run(float dt)
{
    bool running = true;
    while (running)
    {
        {
            displayAll();
        }
        running = handleInput();
        simulateStep();

        //offtime, TODO : set constant fps
        SDL_Delay(dt * 1000);
    }
}

void GraphicalContext::setCenter(vec2 newCenter)
{
    m_viewCenter = newCenter;
}

void GraphicalContext::updateTracks()
{
    vec2 *leftWall;
    vec2 *rightWall;
    m_wallArraysLength = m_tracks->getWalls(&leftWall, &rightWall);

    if (m_wallPoints != nullptr)
    {
        delete[] m_wallPoints;
    }

    m_wallPoints = new vec2[8 * (m_wallArraysLength - 1)];

    vec2 offset(0, 0);
    for (size_t i = 0; i < m_wallArraysLength - 1; i++)
    {
        offset = -m_wallThickness * (leftWall[i + 1] - leftWall[i]).fastOrthRight();
        m_wallPoints[(8 * i) + 0] = leftWall[i];
        m_wallPoints[(8 * i) + 1] = leftWall[i] + offset;
        m_wallPoints[(8 * i) + 2] = leftWall[i + 1] + offset;
        m_wallPoints[(8 * i) + 3] = leftWall[i + 1];

        offset = m_wallThickness * (rightWall[i + 1] - rightWall[i]).fastOrthRight();
        m_wallPoints[(8 * i) + 4] = rightWall[i];
        m_wallPoints[(8 * i) + 5] = rightWall[i] + offset;
        m_wallPoints[(8 * i) + 6] = rightWall[i + 1] + offset;
        m_wallPoints[(8 * i) + 7] = rightWall[i + 1];
    }

    //the start line
    offset = (leftWall[0] - rightWall[0]).fastOrthRight();
    m_beginWall[0] = leftWall[0];
    m_beginWall[1] = rightWall[0];
    m_beginWall[2] = rightWall[0] + offset;
    m_beginWall[3] = leftWall[0] + offset;

    //the begin cap
    vec2 widthLeftRight = rightWall[0] - leftWall[0];
    float width = sqrt(widthLeftRight.sqrLength());
    //we start with the wall parallel to the start line
    m_beginCap[0] = leftWall[0] + width*widthLeftRight.fastOrthRight();
    m_beginCap[1] = m_beginCap[0] + widthLeftRight;
    m_beginCap[2] = m_beginCap[1] + offset;
    m_beginCap[3] = m_beginCap[0] + offset;

    offset = -m_wallThickness * (leftWall[1] - leftWall[0]).orthRight();
    m_beginCap[4] = leftWall[0];
    m_beginCap[5] = leftWall[0] + width*widthLeftRight.fastOrthRight();
    m_beginCap[6] = m_beginCap[5] + offset;
    m_beginCap[7] = m_beginCap[4] + offset;

    m_beginCap[8] = rightWall[0];
    m_beginCap[9] = rightWall[0] + width*widthLeftRight.fastOrthRight();
    m_beginCap[10] = m_beginCap[9] - offset;
    m_beginCap[11] = m_beginCap[8] - offset;


    //the end line
    offset = (leftWall[m_wallArraysLength - 1] - m_wallThickness * leftWall[m_wallArraysLength - 2]).normalize();
    m_endWall[0] = leftWall[m_wallArraysLength - 1];
    m_endWall[1] = leftWall[m_wallArraysLength - 1] + offset;
    m_endWall[2] = rightWall[m_wallArraysLength - 1] + offset;
    m_endWall[3] = rightWall[m_wallArraysLength - 1];

    delete[] leftWall;
    delete[] rightWall;
}

void updateCars()
{

}

// http://slabode.exofire.net/circle_draw.shtml
void drawCircle(float cx, float cy, float r, int num_segments) 
{ 
	glBegin(GL_LINE_LOOP); 
	for(int ii = 0; ii < num_segments; ii++) 
	{ 
		float theta = 2.0f * 3.1415926f * float(ii) / float(num_segments);//get the current angle 

		float x = r * cosf(theta);//calculate the x component 
		float y = r * sinf(theta);//calculate the y component 

		glVertex2f(x + cx, y + cy);//output vertex 

	} 
	glEnd(); 
}


void GraphicalContext::displayAll()
{
    //display

    glClear(GL_COLOR_BUFFER_BIT);

    displayPedestrians();

    displayCars();

    displayWalls();
    
    displaySensors();
        
    displayNN();

    displayGraph();
    
    SDL_GL_SwapWindow(m_window);
}

void GraphicalContext::displayPedestrians()
{
    //Pedestrians
    vec2 pedestrianPoints[4];
    glColor3f(0.0f, 1.0f, 0.0f);
    glBegin(GL_QUADS);
    for (size_t i = 0; i < m_pedestriansLength; i++)
    {
        m_pedestrians[i]->getPoints(pedestrianPoints);
        glVertex2f((m_viewCenter.x - pedestrianPoints[0].x) * m_viewScale, (m_viewCenter.y - pedestrianPoints[0].y) * m_viewScale);
        glVertex2f((m_viewCenter.x - pedestrianPoints[1].x) * m_viewScale, (m_viewCenter.y - pedestrianPoints[1].y) * m_viewScale);
        glVertex2f((m_viewCenter.x - pedestrianPoints[2].x) * m_viewScale, (m_viewCenter.y - pedestrianPoints[2].y) * m_viewScale);
        glVertex2f((m_viewCenter.x - pedestrianPoints[3].x) * m_viewScale, (m_viewCenter.y - pedestrianPoints[3].y) * m_viewScale);
    }
    glEnd();
}

void GraphicalContext::displayCars()
{
    //Cars
    vec2 carPoints[4];
    glColor3f(1.0f, 0.0f, 1.0f);
    glBegin(GL_QUADS);
    for (size_t i = 0; i < m_carsLength; i++)
    {
        m_cars[i]->getPoints(carPoints);
        glVertexWrapperToScreen(carPoints[0]);
        glVertexWrapperToScreen(carPoints[1]);
        glVertexWrapperToScreen(carPoints[2]);
        glVertexWrapperToScreen(carPoints[3]);
    }
    glEnd();
}

void GraphicalContext::displayWalls()
{
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_QUADS);
    //the tracks walls
    for (size_t i = 0; i < 2 * (m_wallArraysLength - 1); i++)
    {
        glVertexWrapperToScreen(m_wallPoints[(4 * i) + 0]);
        glVertexWrapperToScreen(m_wallPoints[(4 * i) + 1]);
        glVertexWrapperToScreen(m_wallPoints[(4 * i) + 2]);
        glVertexWrapperToScreen(m_wallPoints[(4 * i) + 3]);
    }
    glEnd();

    //starting wall
    glColor3f(0.0f, 0.0f, 1.0f);
    glBegin(GL_QUADS);
        glVertexWrapperToScreen(m_beginWall[0]);
        glVertexWrapperToScreen(m_beginWall[1]);
        glVertexWrapperToScreen(m_beginWall[2]);
        glVertexWrapperToScreen(m_beginWall[3]);
    glEnd();

    //ending wall
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_QUADS);
        glVertexWrapperToScreen(m_endWall[0]);
        glVertexWrapperToScreen(m_endWall[1]);
        glVertexWrapperToScreen(m_endWall[2]);
        glVertexWrapperToScreen(m_endWall[3]);
    glEnd();

    //begin cap
    glBegin(GL_QUADS);
    glColor3f(0.0f, 0.0f, 0.0f);
    for (size_t i = 0; i < 3; i++)
    {
        glVertexWrapperToScreen(m_beginCap[(4 * i) + 0]);
        glVertexWrapperToScreen(m_beginCap[(4 * i) + 1]);
        glVertexWrapperToScreen(m_beginCap[(4 * i) + 2]);
        glVertexWrapperToScreen(m_beginCap[(4 * i) + 3]);
    }
    glEnd();
}

void GraphicalContext::displaySensors()
{
    if (m_focusedCarIndex == (size_t)-1) return;
    //we draw the captors
    //Cars
    vec2 carPoints[4];
    std::vector<vec2> sensorEnds =  m_cars[m_focusedCarIndex]->getIntersections();
    glBegin(GL_LINES);
    for (size_t i = 0; i < sensorEnds.size(); i++)
    {
        auto dist = 0.05*(sensorEnds[i] - m_cars[m_focusedCarIndex]->getPosition()).sqrLength();
        auto colorVal = (1 / (dist + 1));
        glColor3f(colorVal, 0, colorVal);
        
        glVertexWrapperToScreen(sensorEnds[i]);
        glVertexWrapperToScreen(m_cars[m_focusedCarIndex]->getPosition());
    }
    glEnd();
    

    glColor3f(.8f, 0.0f, .8f);
    m_cars[m_focusedCarIndex]->getPoints(carPoints);
    glBegin(GL_QUADS);
        glVertexWrapperToScreen(carPoints[0]);
        glVertexWrapperToScreen(carPoints[1]);
        glVertexWrapperToScreen(carPoints[2]);
        glVertexWrapperToScreen(carPoints[3]);
    glEnd();
}

void GraphicalContext::displayNN()
{
    if (m_focusedCarIndex == (size_t)-1) return;
    // Display Focus Car Neural Network
    float xGlobalOffset = -0.85f;
    float yGlobalOffset = 0.65f/m_viewRatio;
    float xGap = 0.20f;
    float yGap = 0.075f/m_viewRatio;
    auto model = m_cars[m_focusedCarIndex]->getModel();
    const auto layers = model->getLayers();
    
    float xOffset = xGlobalOffset;
    for (size_t x = 0; x < layers.size(); x++)
    {
        float yNextOffset = yGlobalOffset - yGap * layers[x + 1].size() * 0.5f;
        float yOffset = yGlobalOffset - yGap * layers[x].size() * 0.5f;
        for (size_t y = 0; y < layers[x].size() - 1; y++)
        {
            // Draw neuron
            glLineWidth(1.0);
            glColor3f(0.0f, 0.0f, 1.0f);
            drawCircle(x * xGap + xOffset, y * yGap + yOffset, 0.020f, 50);

            // Draw Synapses
            auto synapses = layers[x][y]->getSynapses();
            for (size_t s = 0; s < synapses.size(); s++)
            {
                glLineWidth(synapses[s].weight * 4.0);
                if(synapses[s].weight >= 0.0)
                    glColor3f(0.0f, 1.0f, 0.0f);
                else
                    glColor3f(1.0f, 0.0f, 0.0f);

                glBegin(GL_LINES);
                    glVertex2f(x * xGap + xOffset, y * yGap + yOffset);
                    glVertex2f((x + 1) * xGap + xOffset, s * yGap + yNextOffset);
                glEnd();
            }
        }
    }
}

void GraphicalContext::displayGraph()
{
    if (m_simulationStack->size() == 1) return;
    // Display Focus Car Neural Network
    vec2 origin = vec2(0.3,0.3);
    glColor3f(0.0f, 0.0f,.0f);
    glBegin(GL_LINES);
        glVertex2f(origin.x,origin.y/m_viewRatio);
        glVertex2f(origin.x+0.62,origin.y/m_viewRatio);
        glVertex2f(origin.x,origin.y/m_viewRatio);
        glVertex2f(origin.x,(origin.y+0.62)/m_viewRatio);
    glEnd();

    //caching data if new simulation data
    if(m_maxFitnessLog.size() !=  (m_simulationStack->size()-1)*m_saveSpace)
    {
        double* maxFit = (*m_simulationStack)[m_simulationStack->size()-1]->getMaxFitness();
        double* avgFit = (*m_simulationStack)[m_simulationStack->size()-1]->getAvgFitness();
        for (size_t i = 0; i < m_saveSpace; i++)
        {
            m_maxFitnessLog.push_back(maxFit[i]);
            m_avgFitnessLog.push_back(avgFit[i]);
        }
        //sampling
        m_maxGraphPoints[0] = origin;
        m_avgGraphPoints[0] = origin;
        size_t samplingRate = m_maxFitnessLog.size() < 100 ? 1 : m_maxFitnessLog.size()/m_saveSpace;

        for (size_t i = 0; i < (m_maxFitnessLog.size() >= 100 ? 100 : m_maxFitnessLog.size()); i++)
        {
            m_maxGraphPoints[i+1].x = origin.x + 0.006 * (i+1);
            m_maxGraphPoints[i+1].y = m_maxFitnessLog[i*samplingRate];
            m_avgGraphPoints[i+1].x = origin.x + 0.006 * (i+1);
            m_avgGraphPoints[i+1].y = m_avgFitnessLog[i*samplingRate];
        }
        if (m_maxFitnessLog.size() < 100)
        {
            for (size_t i = m_maxFitnessLog.size(); i < 100; i++)
            {
                m_maxGraphPoints[i+1].x = origin.x + 0.006 * (i+1);
                m_maxGraphPoints[i+1].y = m_maxFitnessLog[m_maxFitnessLog.size()-1];
                m_avgGraphPoints[i+1].x = origin.x + 0.006 * (i+1);
                m_avgGraphPoints[i+1].y = m_avgFitnessLog[m_avgFitnessLog.size()-1];
            }
        }

        //normalising
        float maxValue = m_maxGraphPoints[0].y;
        float minValue = m_avgGraphPoints[0].y;
        for (size_t i = 0; i < 101; i++)
        {
            if( m_maxGraphPoints[i].y < minValue) minValue = m_maxGraphPoints[i].y;
            if( m_maxGraphPoints[i].y > maxValue) maxValue = m_maxGraphPoints[i].y;
            if( m_avgGraphPoints[i].y < minValue) minValue = m_avgGraphPoints[i].y;
            if( m_avgGraphPoints[i].y > maxValue) maxValue = m_avgGraphPoints[i].y;
        }
        float scale = 0.6/(maxValue-minValue);
        for (size_t i = 0; i < 101; i++)
        {
            m_avgGraphPoints[i].y *= scale; 
            m_maxGraphPoints[i].y *= scale;
            m_avgGraphPoints[i].y += origin.y+0.6; 
            m_maxGraphPoints[i].y += origin.y+0.6;
            m_avgGraphPoints[i].y /= m_viewRatio; 
            m_maxGraphPoints[i].y /= m_viewRatio;
        }
        

    }

    glColor3f(1.0f, 0.0f,.0f);
    glBegin(GL_LINES);
    for (size_t i = 1; i < 100; i++)
    {
        glVertex2f(m_maxGraphPoints[i].x,m_maxGraphPoints[i].y);
        glVertex2f(m_maxGraphPoints[i+1].x,m_maxGraphPoints[i+1].y);
    }
    glEnd();

    glColor3f(0.0f, 0.0f,1.0f);
    glBegin(GL_LINES);
    for (size_t i = 1; i < 100; i++)
    {
        glVertex2f(m_avgGraphPoints[i].x,m_avgGraphPoints[i].y);
        glVertex2f(m_avgGraphPoints[i+1].x,m_avgGraphPoints[i+1].y);
    }
    glEnd();

    glColor3f(0.0f, 1.0f,0.0f);
    glBegin(GL_LINES);
        glVertex2f(origin.x+0.01+0.60*(((float)m_saveStackIndex)/((float) (m_simulationStack->size()-1))),origin.y/m_viewRatio);
        glVertex2f(origin.x+0.01+0.60*(((float)m_saveStackIndex)/((float) (m_simulationStack->size()-1))),(origin.y+0.62)/m_viewRatio);
    glEnd();
}

void GraphicalContext::glVertexWrapperToScreen(vec2 vert)
{
    glVertex2f((m_viewCenter.x - vert.x) * m_viewScale,
               (m_viewCenter.y - vert.y) * m_viewScale);
}

bool GraphicalContext::handleInput()
{
    //this gets all the events since last event
    m_eventHandler.updateInputs();

    //visualisation
    if (m_eventHandler.isPressed(SDLK_ESCAPE) || m_eventHandler.isWindowsClosed())
    {
        m_simulation->stop();
        return false;
    }
    if (m_eventHandler.isPressed(SDLK_z))
    {
        if (m_focusedCarIndex != (size_t)-1)
            m_focusedCarIndex = -1;
        m_viewCenter.y -= 0.05 / m_viewScale;
    }
    if (m_eventHandler.isPressed(SDLK_s))
    {
        if (m_focusedCarIndex != (size_t)-1)
            m_focusedCarIndex = -1;
        m_viewCenter.y += 0.05 / m_viewScale;
    }
    if (m_eventHandler.isPressed(SDLK_q))
    {
        if (m_focusedCarIndex != (size_t)-1)
            m_focusedCarIndex = -1;
        m_viewCenter.x += 0.05 / m_viewScale;
    }
    if (m_eventHandler.isPressed(SDLK_d))
    {
        if (m_focusedCarIndex != (size_t)-1)
            m_focusedCarIndex = -1;
        m_viewCenter.x -= 0.05 / m_viewScale;
    }
    if (m_eventHandler.isPressed(SDLK_KP_PLUS))
    {
        m_viewScale *= 1.1;
    }
    if (m_eventHandler.isPressed(SDLK_KP_MINUS))
    {
        m_viewScale *= 0.95;
    }
    int32_t wheelValue;
    if(m_eventHandler.wheelMoved(wheelValue))
    {
        if (wheelValue > 0)
        {
            m_viewScale *= 1.2;
        }
        else
        {
            m_viewScale *= 0.85;
        }
    }

    //here we handle focusing on car
    if (!m_nbpush && m_eventHandler.isPressed(SDLK_n))
    {
        if (m_focusedCarIndex == (size_t)-1)
            m_focusedCarIndex = 0;
        else
        {
            m_focusedCarIndex += 1;
            if (m_focusedCarIndex == m_carsLength)
                m_focusedCarIndex = 0;
        }
        m_nbpush = true;
    }
    if (!m_nbpush && m_eventHandler.isPressed(SDLK_b))
    {
        if (m_focusedCarIndex == (size_t)-1)
            m_focusedCarIndex = m_carsLength - 1;
        else
        {
            m_focusedCarIndex -= 1;
            if (m_focusedCarIndex == (size_t)-1)
                m_focusedCarIndex = m_carsLength - 1;
        }
        m_nbpush = true;
    }
    if (m_nbpush && !m_eventHandler.isPressed(SDLK_b) && !m_eventHandler.isPressed(SDLK_n))
    {
        m_nbpush = false;
    }
    if (m_focusedCarIndex != (size_t)-1)
    {
        m_viewCenter = m_cars[m_focusedCarIndex]->getPosition();
    }
    //selecting by mouse
    vec2 coor;
    if(!m_clicking && m_eventHandler.isClick(coor))
    {
        coor.x = ((coor.x/WIDTH)*2) - 1;
        coor.y = - (((coor.y/HEIGHT)*2) - 1);
        coor = m_viewCenter -(coor/m_viewScale) ;
        m_clicking = true;
        size_t minID = 0;
        float minDist = (m_cars[0]->getPosition() - coor).sqrLength();
        float dist = 0;
        for (size_t i = 1; i < m_cars.size(); i++)
        {
            dist = (m_cars[i]->getPosition() - coor).sqrLength();
            if(dist < minDist)
            {
                minDist = dist;
                minID = i;
            }
        }
        m_focusedCarIndex = minID;
        m_viewCenter = m_cars[m_focusedCarIndex]->getPosition();
        
    }
    else if(m_clicking && !m_eventHandler.isClick(coor)) m_clicking = false;

    //regenerate tracks
    if (!m_rpush && m_eventHandler.isPressed(SDLK_r))
    {
        (*m_simulationStack)[m_saveStackIndex]->reach(0);
        updateTracks();
        m_rpush = true;
    }
    if (m_rpush && !m_eventHandler.isPressed(SDLK_r))
    {
        m_rpush = false;
    }

    //change current generation
    if (!m_upPush && m_eventHandler.isPressed(SDLK_UP))
    {
        if(m_saveStackIndex == m_simulationStack->size() - 1)
        {
            m_saveStackIndex =0;
        }
        else m_saveStackIndex += 1;
        
        (*m_simulationStack)[m_saveStackIndex]->reach(0);
        m_tracks = (*m_simulationStack)[m_saveStackIndex]->getTracks();
        m_cars = (*m_simulationStack)[m_saveStackIndex]->getCars();
        m_pedestrians = (*m_simulationStack)[m_saveStackIndex]->getPedestrians();
        std::cout<< "Set generation to " << m_saveStackIndex*m_saveSpace << std::endl;
        updateTracks();
        m_upPush = true;
    }
    if (m_upPush && !m_eventHandler.isPressed(SDLK_UP))m_upPush = false;
    if (!m_downPush && m_eventHandler.isPressed(SDLK_DOWN))
    {
        if(m_saveStackIndex == 0)
        {
            m_saveStackIndex = m_simulationStack->size()-1;
        }
        else m_saveStackIndex -= 1;

        (*m_simulationStack)[m_saveStackIndex]->reach(0);
        m_tracks = (*m_simulationStack)[m_saveStackIndex]->getTracks();
        m_cars = (*m_simulationStack)[m_saveStackIndex]->getCars();
        m_pedestrians = (*m_simulationStack)[m_saveStackIndex]->getPedestrians();
        std::cout<< "Set generation to " << m_saveStackIndex*m_saveSpace << std::endl;
        updateTracks();
        m_downPush = true;
    }
    if (m_downPush && !m_eventHandler.isPressed(SDLK_DOWN))m_downPush = false;

    //pause display
    if (!m_ppush && m_eventHandler.isPressed(SDLK_p))
    {
        m_paused = !m_paused;
        m_ppush =true;
    }
    if (m_ppush && !m_eventHandler.isPressed(SDLK_p))m_ppush = false;

    //pause simulation
    if (!m_hpush && m_eventHandler.isPressed(SDLK_h))
    {
        m_simulation->haltSwitch();
        m_hpush =true;
    }
    if (m_hpush && !m_eventHandler.isPressed(SDLK_h))m_hpush = false;

    //step time
    if (!m_rightPush && m_eventHandler.isPressed(SDLK_RIGHT))
    {
        (*m_simulationStack)[m_saveStackIndex]->stepCycle();
        m_rightPush = true;
    }
    if (m_rightPush && !m_eventHandler.isPressed(SDLK_RIGHT))m_rightPush = false;
    if (!m_leftPush && m_eventHandler.isPressed(SDLK_LEFT))
    {
        (*m_simulationStack)[m_saveStackIndex]->stepBackCycle();
        m_leftPush = true;
    }
    if (m_leftPush && !m_eventHandler.isPressed(SDLK_LEFT))m_leftPush = false;

    return true;
}

void GraphicalContext::simulateStep()
{
    if(!m_paused)
    {
        if(m_forward)
        {
            (*m_simulationStack)[m_saveStackIndex]->stepCycle();
        }
        else
        {
            (*m_simulationStack)[m_saveStackIndex]->stepBackCycle();
        }
    }
}