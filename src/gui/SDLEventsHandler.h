#ifndef SDLEVENTSHANDLER_H
#define SDLEVENTSHANDLER_H

#include "common.h"

class SDLEventsHandler
{
    public :
        SDLEventsHandler();
        ~SDLEventsHandler();

        void updateInputs();

        bool isPressed(SDL_Keycode);
        bool isWindowsClosed();

        bool isClick(vec2& coor);

        bool wheelMoved(int32_t& mov);

    private :
        std::map<SDL_Keycode,bool> m_keyboardPressMap;
        bool m_windowsClosedEvent;
        
        bool m_click;
        vec2 m_clickCoordinates;
        SDL_Event m_currentEvent;
        int32_t m_wheelmov;
};


#endif