#ifndef COMMON_H
#define COMMON_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <algorithm>
#include <thread>
#include <mutex>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ostream>
#include <map>
#include <chrono>
#include <vector>
#include <memory>
#include "squirrel3.h"


class vec2
{
    public :
        float x;
        float y;
        vec2();
        vec2(float sx,float sy);

        //beautifull overloaded operators, for convinience sake
        vec2 operator+(const vec2 & a) const;
        vec2 operator-(const vec2 & a) const;
        float operator*(const vec2 & a) const;
        vec2 operator*(const float & a) const;
        vec2 operator/(const float & a) const;
        vec2& operator+=(const vec2 & a);
        vec2& operator-=(const vec2 & a);
        friend vec2 operator*(const float &a,const vec2& b);
        friend std::ostream& operator<<(std::ostream& o,const vec2& vect);

        //return the normalised vector associated with base, costly
        vec2 normalize();

        //return normalizeed vector using fast inverse square root, cheap but less precise (few percent loss)
        vec2 fastNormalize();

        //rotates a vector anti-clockwise, angle in rad
        vec2 rotate(float angle);

        //returns the vector orthogonal to base, to its right, with norm 1
        vec2 orthRight();      
        //using fastnormalize :
        vec2 fastOrthRight();

        float sqrLength();
};

bool intersect(vec2,vec2,vec2,vec2);
bool intersectPoint(vec2,vec2,vec2,vec2,vec2&);

extern PRNG g_randomGenerator;

float getRandomTo01();
float getRandomTo01(float offset);
float getRandomBetween(float min, float max);
void setWeightsToFile(std::vector<double> weights, int numNeuron, std::string fileName, int generation);
std::vector<double> getWeightsFromFile(std::string weightsFile, int neuronNum);

#endif