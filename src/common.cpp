#include "common.h"

PRNG g_randomGenerator(666);

// return a random between 0 + offset and 1 + offset
float getRandomTo01(float offset)
{
    return getRandomTo01() + offset;
}
// return a random between 0 and 1
float getRandomTo01()
{
    return PRNG::randomTo01(g_randomGenerator.random32());
}
// return a random between min and max
float getRandomBetween(float min, float max)
{
    return getRandomTo01() * abs(max - min) + min;
}

void setWeightsToFile(std::vector<double> weights, int neuronNum, std::string fileName, int generation)
{
    fileName += std::to_string(generation).c_str();
    std::fstream myFile;
    if(neuronNum == 0)
        myFile.open(fileName, std::ofstream::out | std::ofstream::trunc);
    else
        myFile.open(fileName, std::ofstream::out | std::ofstream::app);
    if (!myFile) {
		std::cout << "File not created!\n";
	}
    myFile << neuronNum << ":";   
    for (size_t i = 0; i < weights.size(); i++)
    {
        myFile << " " + std::to_string(weights[i]);   
    }
    myFile << "\n";   
    myFile.close();
}

std::vector<double> getWeightsFromFile(std::string weightsFile, int neuronNum)
{
    std::vector<double> res;
    std::ifstream weightsFileStream;
    weightsFileStream.open(weightsFile.c_str());

    for (std::string line; std::getline(weightsFileStream, line); )
    {
        std::stringstream ss(line);
        std::string label;
        ss >> label;
        if (label.compare(std::to_string(neuronNum) + ":") == 0) 
        {
            double oneValue;
            while (ss >> oneValue) 
            {
                res.emplace_back(oneValue);
            }
            weightsFileStream.close();
            return res;
        }
    }
    weightsFileStream.close();
    return res;
}

vec2::vec2()
{
    x = 0;
    y = 0;
}

vec2::vec2(float sx, float sy)
{
    x = sx;
    y = sy;
}

vec2 vec2::operator+(const vec2 & a) const
{
    return vec2(a.x+x,a.y+y);
}
vec2 vec2::operator-(const vec2 & a) const
{
    return vec2(x-a.x,y-a.y);
}
float vec2::operator*(const vec2 & a) const
{
    return (a.x*x)+(a.y*y);
}
vec2 vec2::operator*(const float & a) const
{
    return vec2(x*a,y*a);
}
vec2 vec2::operator/(const float & a) const
{
    return vec2(x/a,y/a);
}
vec2 operator*(const float &a,const vec2& b)
{
    return vec2(a*b.x,a*b.y);
}
vec2& vec2::operator+=(const vec2 & a) 
{
    x+=a.x;
    y+=a.y;
    return *this;
}
vec2& vec2::operator-=(const vec2 & a) 
{
    x-=a.x;
    y-=a.y;
    return *this;
}
std::ostream& operator<<(std::ostream& o,const vec2& vect)
{
    o << "(" << vect.x <<","<<vect.y<<")";
    return o;
}

vec2 vec2::normalize()
{
    float length = sqrt((x*x) + (y*y));
    return vec2(x/length,y/length);
}

//using legendary Quake 3 algorithm
vec2 vec2::fastNormalize()
{
    float invSqrt = (x*x) + (y*y);
    long i;
    float tmp;
    const float threehalfs = 1.5F;

    tmp = invSqrt * 0.5F;
    i  = * ( long * ) &invSqrt;    
    i  = 0x5f3759df - ( i >> 1 );                
    invSqrt  = * ( float * ) &i;
    invSqrt  = invSqrt * ( threehalfs - ( tmp * invSqrt * invSqrt ) );  
    invSqrt  = invSqrt * ( threehalfs - ( tmp * invSqrt * invSqrt ) );   

    return vec2(x*invSqrt,y*invSqrt);
}

vec2 vec2::rotate(float angle)
{
    return vec2(( x * cos(angle)) - ( y * sin(angle)),( x * sin(angle)) + ( y * cos(angle)));
};

vec2 vec2::orthRight()
{
    return vec2(y,- x).normalize();
}

vec2 vec2::fastOrthRight()
{
    return vec2(y,- x).fastNormalize();
}

float vec2::sqrLength()
{
    return (x*x + y*y);
}


//algorithm by jeffrey thompson
bool intersect(vec2 a1, vec2 a2,vec2 b1, vec2 b2)
{
    float uA = ((b2.x-b1.x)*(a1.y-b1.y) - (b2.y-b1.y)*(a1.x-b1.x)) / ((b2.y-b1.y)*(a2.x-a1.x) - (b2.x-b1.x)*(a2.y-a1.y));
    float uB = ((a2.x-a1.x)*(a1.y-b1.y) - (a2.y-a1.y)*(a1.x-b1.x)) / ((b2.y-b1.y)*(a2.x-a1.x) - (b2.x-b1.x)*(a2.y-a1.y));
    if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) 
    {
        return true;
    }
    return false;
}

bool intersectPoint(vec2 a1, vec2 a2,vec2 b1, vec2 b2,vec2& ret)
{
    float uA = ((b2.x-b1.x)*(a1.y-b1.y) - (b2.y-b1.y)*(a1.x-b1.x)) / ((b2.y-b1.y)*(a2.x-a1.x) - (b2.x-b1.x)*(a2.y-a1.y));
    float uB = ((a2.x-a1.x)*(a1.y-b1.y) - (a2.y-a1.y)*(a1.x-b1.x)) / ((b2.y-b1.y)*(a2.x-a1.x) - (b2.x-b1.x)*(a2.y-a1.y));
    if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) 
    {
        ret.x = a1.x  + (uA * (a2.x - a1.x));
        ret.y = a1.y  + (uA * (a2.y - a1.y));
        return true;
    }
    return false;
}