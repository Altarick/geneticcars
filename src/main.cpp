#include "common.h"

#include "gui/GraphicalContext.h"
#include "simulation/Tracks.h"
#include "simulation/Car.h"
#include "simulation/GeneticAlgorithm.h"
#include "simulation/SavedSimulation.h"
#include "simulation/SimulationBank.h"

#include <cassert>
#include <filesystem>

#include <neural_network/activation_functions.h>
#include <neural_network/neural_network.h>
#include <neural_network/training_data.h>

void dispValues(const std::string label, const double &value)
{
    // Disp label
    std::cout << label << " ";
    // Disp values
    std::cout << value << " ";
    // Disp end
    std::cout << std::endl;
}

void dispValues(const std::string label, const std::vector<double> &values)
{
    // Disp label
    std::cout << label << " ";
    // Disp values
    for (size_t i = 0; i < values.size(); i++)
    {
        std::cout << values[i] << " ";
    }
    // Disp end
    std::cout << std::endl;
}

template <typename T>
void inputToParameter(std::string label, T &t)
{
    std::cout << label << std::endl;
    std::cin >> t;
    while (std::cin.fail())
    {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << label << std::endl;
        std::cin >> t;
    }
}

void populate(std::vector<Pedestrian*>& pedestrians, size_t pedestrianNumber, Tracks& parcours)
{
    for (size_t i = 0; i < pedestrianNumber; i++)
    {
        pedestrians[i] = new Pedestrian({1, 2}, vec2(),  vec2(1,0), parcours.getWidth());
    }
}

int main(int argc, char** argv)
{
    if(argc > 2) return -1;
    // Randomness setup
    auto now = std::chrono::system_clock::now();
    auto seed = std::chrono::time_point_cast<std::chrono::milliseconds>(now).time_since_epoch().count();
    g_randomGenerator.setSeed(seed);

    // Track building
    Tracks parcours(10, 20, 6, .9);

    // Pedestrians
    int pedestrianNumber = 6;
    std::vector<Pedestrian*> pedestrians(pedestrianNumber);
    populate(pedestrians, pedestrianNumber, parcours);

    // Setup network topology
    TrainingData trainData("./tmp/training_data_example.txt");
    std::vector<size_t> topology;
    trainData.getTopology(topology);
    assert(topology[0] > 1 && topology[0] < 50); // avoid excess with sensors number

    float sensorDenseAngle = 0.3;
    size_t sensorRecursionLevel = 2;

    topology[0] = 6;
    size_t mult = 1;
    for (size_t i = 0; i < sensorRecursionLevel; i++)
    {
        mult*=4;
    }
    if(mult != 1)  topology[0] += mult;

    // Cars and NN generations
    std::vector<Car *> cars = std::vector<Car *>();
    std::vector<NNetwork *> models = std::vector<NNetwork *>();
    for (size_t i = 0; i < 20; i++)
    {
        NNetwork * carModel = nullptr;
        if(argc == 2)
        {
            carModel = new NNetwork(argv[1], topology, &fTanh, &dTanh, &fTanh, &dTanh);
        }
        else
        {
            carModel = new NNetwork(topology, &fTanh, &dTanh, &fTanh, &dTanh);
        }
        models.push_back(carModel);
        cars.push_back(new Car({1, 2}, {0, 0}, {1, 0}, carModel,sensorRecursionLevel,sensorDenseAngle ));
    }
    //save stack
    SimulationBank saveStack = SimulationBank();
    //genetic algorithm
    GeneticAlgorithm breeder = GeneticAlgorithm(cars,&parcours,0.01);
    
    size_t saveSpacing = 100;
    float dt = 0.030f;
    // Simulation launching
    Simulation mainSimul(&parcours,cars,pedestrians,&saveStack,saveSpacing,0.2,700,&breeder);
    
    //std::thread simulationThread(&Simulation::simulateGenerations, &mainSimul,1001);
    std::thread simulationThread(&Simulation::simulateGenerationsEndless, &mainSimul);

    while (saveStack.size() == 0)
    {
        SDL_Delay(250);
    }
    

    GraphicalContext graphicalUI(&saveStack,saveSpacing,&mainSimul);
    graphicalUI.run(dt);

    simulationThread.join();
    std::cout << "... done showing off" << std::endl;

    // Cars deallocation
    while (cars.size() > 0)
    {
        delete cars.back();
        cars.pop_back();
    }

    // Models deallocation
    while (models.size() > 0)
    {
        delete models.back();
        models.pop_back();
    }

    return 0;
}