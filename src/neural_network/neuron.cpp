#include <neural_network/neuron.h>

#include <cmath>
#include <cassert>
#include <memory>

#include <neural_network/neural_layer.h>
#include <neural_network/neural_synapse.h>

Neuron::Neuron(size_t numOutputs
                , size_t index
                , std::function<double(double)> activationFunction
                , std::function<double(double)> activationFunctionDerivative) : 
    m_index(index),
    m_outputValue(0.0),
    m_gradient(0.0),
    activationFunction(activationFunction),
    activationFunctionDerivative(activationFunctionDerivative)
{
    for (size_t i = 0; i < numOutputs; i++)
    {
        auto synapse = NSynapse();
        synapse.weight = randomWeight();
        synapse.deltaWeight = 0.0;
        m_outputWeights.emplace_back(synapse);
    }
}

Neuron::~Neuron()
{

}

void Neuron::setOutputWeights(std::vector<double> vWeights)
{
    assert(vWeights.size() == m_outputWeights.size());
    for (size_t i = 0; i < vWeights.size(); i++)
    {
        m_outputWeights[i].weight = vWeights[i];
    }
}

void Neuron::propagateForward(const NLayer& previousLayer)
{
    double sum = 0.0;
    for (size_t i = 0; i < previousLayer.size(); i++)
    {
        sum += previousLayer[i]->getOutputValue() *
                previousLayer[i]->m_outputWeights[m_index].weight;
    }
    m_outputValue = activationFunction(sum);
}

void Neuron::calculateOutputGradient(double targetValue)
{
    double delta = targetValue - m_outputValue;
    m_gradient = delta * activationFunctionDerivative(m_outputValue);
}

void Neuron::calculateHiddenGradient(const NLayer& nextLayer)
{
    double sum = 0.0;
    for (size_t i = 0; i < nextLayer.size() - 1; i++)
    {
        sum += m_outputWeights[i].weight * nextLayer[i]->m_gradient;
    }
    m_gradient = sum * activationFunctionDerivative(m_outputValue);
}

void Neuron::updateInputWeights(NLayer& previousLayer, double& eta, double& alpha)
{
    for (size_t i = 0; i < previousLayer.size(); i++)
    {
        auto neuron = previousLayer[i];
        double oldDeltaWeight = neuron->m_outputWeights[m_index].deltaWeight;
        double newDeltaWeight = eta
                                * neuron->getOutputValue()
                                * m_gradient
                                + alpha
                                * oldDeltaWeight;
        neuron->m_outputWeights[m_index].deltaWeight = newDeltaWeight;
        neuron->m_outputWeights[m_index].weight += newDeltaWeight; 
    }
}

void Neuron::setGeneticCode(double* code)
{
    for (size_t i = 0; i < m_outputWeights.size(); i++)
    {
        m_outputWeights[i].weight = code[i];
    }
}

void Neuron::getGeneticCode(double* code)
{
    for (size_t i = 0; i < m_outputWeights.size(); i++)
    {
        code[i] = m_outputWeights[i].weight ;
    }
}

size_t Neuron::getGeneticCodeSize()
{
    return m_outputWeights.size();
}

