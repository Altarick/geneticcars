#pragma once

#include <cstdlib>
#include <functional>
#include <vector>

#include <common.h>
#include <neural_network/neural_layer.h>
#include <neural_network/neural_synapse.h>

class Neuron
{
public:
    Neuron(size_t numOutputs
                , size_t index
                , std::function<double(double)> activationFunction
                , std::function<double(double)> activationFunctionDerivative);
    ~Neuron();

    void setOutputValue(double value) { m_outputValue = value; }
    double getOutputValue() const { return m_outputValue; }

    void setOutputWeights(std::vector<double> vWeights);

    void propagateForward(const NLayer &previousLayer);

    void calculateOutputGradient(double targetValue);
    void calculateHiddenGradient(const NLayer& nextLayer);
    void updateInputWeights(NLayer& previousLayer, double& eta, double& alpha);

    const std::vector<NSynapse>& getSynapses() const { return m_outputWeights; }

    void setGeneticCode(double* code);
    void getGeneticCode(double* code);
    size_t getGeneticCodeSize();

private:
    static double randomWeight() { return getRandomBetween(-1, 1); }

    std::vector<NSynapse> m_outputWeights;
    size_t m_index;
    double m_outputValue;
    double m_gradient;
    std::function<double(double)> activationFunction;
    std::function<double(double)> activationFunctionDerivative;
};