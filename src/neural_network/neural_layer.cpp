#include "neural_layer.h"
#include "neuron.h"

NLayer::NLayer() : m_data(),m_neuronsCodeSize()
{

}

NLayer::NLayer(const NLayer& toCopy) : m_data()
{
    for (size_t i = 0; i < toCopy.m_data.size(); i++)
    {
        std::shared_ptr<Neuron> t(new Neuron(*(toCopy.m_data[i])));
        m_data.push_back(t);
    }   
}



NLayer::~NLayer()
{
    
}

NLayer& NLayer::operator=(const NLayer& toCopy)
{
    m_data.resize(toCopy.m_data.size());
    for (size_t i = 0; i < m_data.size(); i++)
    {
        std::shared_ptr<Neuron> t(new Neuron(*(toCopy.m_data[i])));
        m_data[i] = t; 
    }
    return *this;
}


void NLayer::setGeneticCode(double* code)
{
    if(m_neuronsCodeSize.size() != m_data.size())
    {
        m_neuronsCodeSize.resize(m_data.size());
        for (size_t i = 0; i < m_data.size(); i++)
        {
            m_neuronsCodeSize[i] = m_data[i]->getGeneticCodeSize();
        }
    }
    size_t index = 0;
    for (size_t i = 0; i < m_data.size(); i++)
    {
        m_data[i]->setGeneticCode(&(code[index]));
        index += m_neuronsCodeSize[i];
    }
    
}

void NLayer::getGeneticCode(double* code)
{
    if(m_neuronsCodeSize.size() != m_data.size())
    {
        m_neuronsCodeSize.resize(m_data.size());
        for (size_t i = 0; i < m_data.size(); i++)
        {
            m_neuronsCodeSize[i] = m_data[i]->getGeneticCodeSize();
        }
    }
    size_t index = 0;
    for (size_t i = 0; i < m_data.size(); i++)
    {
        m_data[i]->getGeneticCode(&(code[index]));
        index += m_neuronsCodeSize[i];
    }
}

size_t NLayer::getGeneticCodeSize()
{
    if(m_neuronsCodeSize.size() != m_data.size())
    {
        m_neuronsCodeSize.resize(m_data.size());
        for (size_t i = 0; i < m_data.size(); i++)
        {
            m_neuronsCodeSize[i] = m_data[i]->getGeneticCodeSize();
        }
    }
    size_t sum = 0;
    for (size_t i = 0; i < m_data.size(); i++)
    {
        sum+= m_neuronsCodeSize[i];
    }
    return sum;
}