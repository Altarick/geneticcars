#pragma once

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>

class TrainingData
{
public:
    TrainingData(const std::string filename);
    ~TrainingData();
    
    bool isEof() { return m_trainingDataFile.eof(); }
    void getTopology(std::vector<size_t> &topology);

    size_t getNextInputs(std::vector<double> &inputVals);
    size_t getTargetOutputs(std::vector<double> &targetOutputVals);

private:
    std::ifstream m_trainingDataFile;
};