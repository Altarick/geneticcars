#pragma once
#include "common.h"

class Neuron;
//typedef std::vector<std::shared_ptr<Neuron>> NLayer;

class NLayer
{
    public:
        NLayer(/* args */);
        NLayer(const NLayer& toCopy);
        NLayer& operator=(const NLayer& toCopy);

        ~NLayer();
        
        std::shared_ptr<Neuron>& operator[]( std::size_t i) {return m_data[i];}
        const std::shared_ptr<Neuron>& operator[]( std::size_t i) const {return m_data[i];}

        size_t size() const {return m_data.size();}
        void emplace_back(std::shared_ptr<Neuron> nr) {m_data.emplace_back(nr);}
        std::shared_ptr<Neuron> back() const {return m_data.back();}

        void setGeneticCode(double* code);
        void getGeneticCode(double* code);
        size_t getGeneticCodeSize();

    private:
        std::vector<std::shared_ptr<Neuron>> m_data;
        std::vector<size_t> m_neuronsCodeSize;
};
