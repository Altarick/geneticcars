#include <neural_network/activation_functions.h>

#include <cmath>

double fTanh(double x)
{
    return tanh(x);
}
double dTanh(double x)
{
    return 1 + x * x;
}

double fRelu(double x)
{
    return x < 0 ? 0 : x;
}
double dRelu(double x)
{
    return x < 0 ? 0 : 1;
}

double fSigmoid(double x)
{
    double y = exp(-x);
    return 1.0 / (1.0 + y);
}
double dSigmoid(double x)
{
    double y = exp(-x);
    return pow(y / (1 + y), 2);
}

double fLinear(double x)
{
    return x;
}
double dLinear(double x __attribute__((unused)))
{
    return 1;
}

std::vector<double> fSoftmax(std::vector<double> x)
{
    std::vector<double> y;

    double sum = 0.0;
    for (size_t i = 0; i < x.size();  i++)
    {
        sum += x[i];
    }
    
    for (int i = x.size() - 1; i >= 0;  i--)
    {
        y.push_back(exp(x[i]) / sum);
    }

    return y;
}