#pragma once

#include <functional>
#include <vector>

#include <common.h>
#include <neural_network/neural_layer.h>

class NNetwork
{
public:
    NNetwork(const std::vector<size_t> &topology
                , std::function<double(double)> inputActivationFunction
                , std::function<double(double)> inputActivationFunctionDerivative
                , std::function<double(double)> outputActivationFunction
                , std::function<double(double)> outputActivationFunctionDerivative);
    NNetwork(const std::string weightsFile
                , const std::vector<size_t> &topology
                , std::function<double(double)> inputActivationFunction
                , std::function<double(double)> inputActivationFunctionDerivative
                , std::function<double(double)> outputActivationFunction
                , std::function<double(double)> outputActivationFunctionDerivative);
    NNetwork(const NNetwork& toCopy);
    NNetwork& operator=(const NNetwork& toCopy);
    ~NNetwork();

    void propagateForward(const std::vector<double> &inputValues);
    void propagateBackward(const std::vector<double> &targetValues, double& eta, double& alpha, double& smoothingFactor);
    void getPrediction(std::vector<double> &resultValues) const;
    double getRecentAverageError() const { return m_accuracy; }
    const std::vector<NLayer>& getLayers() const { return m_layers; }

    void setGeneticCode(double* code);
    void getGeneticCode(double* code);
    size_t getGeneticCodeSize();

private:
    std::vector<NLayer> m_layers;
    std::vector<size_t> m_layersCodeSizes;
    double m_error;
    double m_accuracy;
};