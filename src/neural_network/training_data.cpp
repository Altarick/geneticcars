#include <neural_network/training_data.h>

void TrainingData::getTopology(std::vector<size_t> &topology)
{
    std::string line;
    std::string label;

    getline(m_trainingDataFile, line);
    std::stringstream ss(line);
    ss >> label;
    if (this->isEof() || label.compare("topology:") != 0) {
        abort();
    }

    while (!ss.eof()) {
        size_t n;
        ss >> n;
        topology.emplace_back(n);
    }
}

TrainingData::TrainingData(const std::string filename)
{
    m_trainingDataFile.open(filename.c_str());
}

TrainingData::~TrainingData()
{
    m_trainingDataFile.close();
}

size_t TrainingData::getNextInputs(std::vector<double> &inputVals)
{
    inputVals.clear();

    std::string line;
    getline(m_trainingDataFile, line);
    std::stringstream ss(line);

    std::string label;
    ss >> label;
    if (label.compare("in:") == 0) {
        double oneValue;
        while (ss >> oneValue) {
            inputVals.emplace_back(oneValue);
        }
    }
    return inputVals.size();
}

size_t TrainingData::getTargetOutputs(std::vector<double> &targetOutputValues)
{
    targetOutputValues.clear();

    std::string line;
    getline(m_trainingDataFile, line);
    std::stringstream ss(line);

    std::string label;
    ss >> label;
    if (label.compare("out:") == 0) {
        double oneValue;
        while (ss >> oneValue) {
            targetOutputValues.emplace_back(oneValue);
        }
    }
    return targetOutputValues.size();
}
