#pragma once

#include <vector>

double fTanh(double x);
double dTanh(double x);

double fRelu(double x);
double dRelu(double x);

double fSigmoid(double x);
double dSigmoid(double x);

double fLinear(double x);
double dLinear(double x);

std::vector<double> fSoftmax(std::vector<double> x);