#include <neural_network/neural_network.h>

#include <cassert>
#include <cmath>
#include <memory>
#include <iostream>

#include <neural_network/neuron.h>

NNetwork::NNetwork(const std::vector<size_t> &topology
                    , std::function<double(double)> inputActivationFunction
                    , std::function<double(double)> inputActivationFunctionDerivative
                    , std::function<double(double)> outputActivationFunction
                    , std::function<double(double)> outputActivationFunctionDerivative) : 
    m_layersCodeSizes(),
    m_error(0.0),
    m_accuracy(0.0)
{
    m_layers = {};
    auto numLayers = topology.size();

    // Layers setup
    for (size_t i = 0; i < numLayers - 1; i++)
    {
        auto layer = NLayer();
        auto numNeurons = topology[i] + 1;
        auto numOutputs = topology[i + 1];
        for (size_t j = 0; j < numNeurons; j++)
        {
            auto neuron = std::make_shared<Neuron>(numOutputs, j, inputActivationFunction, inputActivationFunctionDerivative);
            layer.emplace_back(neuron);
        }
        // Last layer neuron is the bias
        layer.back()->setOutputValue(1.0);
        // Put layer in neural network

        m_layers.emplace_back(layer);
    }

    // Output Layer
    auto layer = NLayer();
    auto numNeurons = topology[numLayers - 1] + 1; // Useless bias in output layer to simplify code
    auto numOutputs = 0; // no output for the output layer
    for (size_t j = 0; j < numNeurons; j++)
    {
        auto neuron = std::make_shared<Neuron>(numOutputs, j, outputActivationFunction, outputActivationFunctionDerivative);
        layer.emplace_back(neuron);
    }
    // Last layer neuron is the bias
    layer.back()->setOutputValue(1.0);
    // Put layer in neural network
    m_layers.emplace_back(layer);
}

NNetwork::NNetwork(const std::string weightsFile
                    , const std::vector<size_t> &topology
                    , std::function<double(double)> inputActivationFunction
                    , std::function<double(double)> inputActivationFunctionDerivative
                    , std::function<double(double)> outputActivationFunction
                    , std::function<double(double)> outputActivationFunctionDerivative) : 
    NNetwork(topology, inputActivationFunction
            , inputActivationFunctionDerivative
            , outputActivationFunction
            , outputActivationFunctionDerivative)
{
    // Update weights
    int neuronNum = 0;
    for (size_t i = 0; i < m_layers.size() - 1; i++)
    {
        // Get Layer
        auto& layer = m_layers[i];
        for (size_t j = 0; j < layer.size(); j++)
        {
            // Read weights for this neuron
            auto vWeights = getWeightsFromFile(weightsFile, neuronNum);
            auto& neuron = layer[j];
            neuron->setOutputWeights(vWeights);
            neuronNum++;
        }
    }
}

NNetwork::NNetwork(const NNetwork& toCopy):
    m_layers(toCopy.m_layers),
    m_error(toCopy.m_error),
    m_accuracy(toCopy.m_accuracy)
{

}

NNetwork& NNetwork::operator=(const NNetwork& toCopy)
{
    m_layers = toCopy.m_layers;
    m_error = toCopy.m_error;
    m_accuracy = toCopy.m_accuracy;
    return *this;
}

NNetwork::~NNetwork()
{
}

void NNetwork::propagateForward(const std::vector<double> &inputValues)
{
    // assert( inputValues.size() == m_layers[0].size() - 1);

    // Assign each input value to its corresponding input layer neuron
    for (size_t i = 0; i < inputValues.size(); i++)
    {
        m_layers[0][i]->setOutputValue(inputValues[i]);
    }

    // Forward Propagation
    for (size_t l = 1; l < m_layers.size(); l++)
    {
        NLayer &previousLayer = m_layers[l - 1];
        for (size_t n = 0; n < m_layers[l].size() - 1; n++)
        {
            m_layers[l][n]->propagateForward(previousLayer);
        }
    }
}

void NNetwork::propagateBackward(const std::vector<double> &targetValues, double &eta, double &alpha, double &smoothingFactor)
{
    NLayer &outputLayer = m_layers.back();
    m_error = 0.0;

    // Estimate error in output layer (RMS)
    for (size_t i = 0; i < outputLayer.size() - 1; i++)
    {
        double delta = targetValues[i] - outputLayer[i]->getOutputValue();
        m_error += delta * delta;
    }
    m_error /= outputLayer.size() - 1;
    m_error = sqrt(m_error);

    // Recent average error (TODO) 
    m_accuracy = m_accuracy * smoothingFactor + m_error;
    m_accuracy /= (smoothingFactor + 1.0);

    // Calculate Gradient from Output Layer
    for (size_t i = 0; i < outputLayer.size(); i++)
    {
        outputLayer[i]->calculateOutputGradient(targetValues[i]);
    }

    // Calculate Hidden Layer Gradients
    for (size_t i = m_layers.size() - 2; i > 0; i--)
    {
        NLayer &hiddenLayer = m_layers[i];
        NLayer &nextLayer = m_layers[i + 1];
        for (size_t j = 0; j < hiddenLayer.size(); j++)
        {
            hiddenLayer[j]->calculateHiddenGradient(nextLayer);
        }
    }

    // Weights updates
    for (size_t i = m_layers.size() - 1; i > 0; i--)
    {
        NLayer &currentLayer = m_layers[i];
        NLayer &previousLayer = m_layers[i - 1];
        for (size_t j = 0; j < currentLayer.size() - 1; j++)
        {
            currentLayer[j]->updateInputWeights(previousLayer, eta, alpha);
        }
    }
}

void NNetwork::getPrediction(std::vector<double> &resultValues) const
{
    resultValues.clear();
    for (size_t i = 0; i < m_layers.back().size() - 1; i++)
    {
        resultValues.emplace_back(m_layers.back()[i]->getOutputValue());
    }
}

void NNetwork::setGeneticCode(double* code)
{
    if(m_layersCodeSizes.size() != m_layers.size())
    {
        m_layersCodeSizes.resize(m_layers.size());
        for (size_t i = 0; i < m_layers.size(); i++)
        {
            m_layersCodeSizes[i] = m_layers[i].getGeneticCodeSize();
        }
    }
    size_t index = 0;
    for (size_t i = 0; i < m_layersCodeSizes.size(); i++)
    {
        m_layers[i].setGeneticCode(&(code[index]));
        index += m_layersCodeSizes[i];
    }
    
}

void NNetwork::getGeneticCode(double* code)
{
    if(m_layersCodeSizes.size() != m_layers.size())
    {
        m_layersCodeSizes.resize(m_layers.size());
        for (size_t i = 0; i < m_layers.size(); i++)
        {
            m_layersCodeSizes[i] = m_layers[i].getGeneticCodeSize();
        }
    }
    size_t index = 0;
    for (size_t i = 0; i < m_layers.size(); i++)
    {
        m_layers[i].getGeneticCode(&(code[index]));
        index += m_layersCodeSizes[i];
    }
}

size_t NNetwork::getGeneticCodeSize()
{
    if(m_layersCodeSizes.size() != m_layers.size())
    {
        m_layersCodeSizes.resize(m_layers.size());
        for (size_t i = 0; i < m_layers.size(); i++)
        {
            m_layersCodeSizes[i] = m_layers[i].getGeneticCodeSize();
        }
    }
    size_t sum = 0;
    for (size_t i = 0; i < m_layers.size(); i++)
    {
        sum+= m_layersCodeSizes[i];
    }
    return sum;
}