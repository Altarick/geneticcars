#pragma once

struct NSynapse
{
    NSynapse() : weight(0.0), deltaWeight(RAND_MAX)
    {
    }

    double weight = 0.0;
    double deltaWeight = RAND_MAX;
};