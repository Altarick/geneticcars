#ifndef PRNG_H
#define PRNG_H
#include <cstdint> //for os-independent uint32

//this implements a Quirrel3 generator based on work from Squirrel Eiserloh https://www.smu.edu/Guildhall/People/Faculty/Squirrel-Eiserloh
class PRNG
{
    public :
        /*
        This creates a pseudo-random number generator based on a noise created by bitshifting
        Suprisingly fast an efficient
        */
        PRNG(uint32_t seed);

        //change seed
        void setSeed(uint32_t seed);

        //return a random 32 bit number
        uint32_t random32();
        //returns noise at location
        uint32_t random32(uint32_t position); 

        //forcefully reset the position cursor to replay random sequence 
        void setPositionInSeed(uint32_t toSet);

        //this maps uint32 range to [0,1]
        static float randomTo01(uint32_t value);

    protected :
        uint32_t m_seed;
        uint32_t m_position;

        //magic numbers
        const uint32_t m_BIT_NOISE1 = 0x68E31DA4;
        const uint32_t m_BIT_NOISE2 = 0xB5297A4D;
        const uint32_t m_BIT_NOISE3 = 0x1B56C4E9;

};


#endif
