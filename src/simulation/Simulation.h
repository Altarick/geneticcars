#ifndef SIMULATION_H
#define SIMULATION_H

#include "common.h"
#include "simulation/Car.h"
#include "simulation/Tracks.h"
#include "simulation/Pedestrian.h"
#include "simulation/SavedSimulation.h"
#include "simulation/SimulationBank.h"
#include "simulation/GeneticAlgorithm.h"

class Simulation
{
    public:
        Simulation( Tracks *, 
                    std::vector<Car *>,
                    std::vector<Pedestrian *>,
                    SimulationBank*,
                    size_t saveSpacing,
                    float dt,
                    size_t stepsPerGeneration,
                    GeneticAlgorithm* genAlg);
        ~Simulation();

        void simulateGeneration();
        void simulateGenerations(size_t n);
        void simulateGenerationsEndless();

        void stop();
        void haltSwitch();

    private:
        //thread management
        bool m_stop;
        bool m_halt;
        std::mutex m_mutex;

        //the tracks, for collision purpose
        Tracks *m_tracks;
        vec2 *m_leftWall;
        vec2 *m_rightWall;
        vec2* m_beginCap; //those are the 3 walls behind the start
        vec2 *m_beginWall;
        vec2 *m_endWall;
        void updateTracks();
        size_t m_wallArraysLength;


        //the cars
        std::vector<Car *> m_cars;
        size_t m_carsLength;
        void resetAllCars();

        //the pedestrians
        std::vector<Pedestrian *> m_pedestrians;
        size_t m_pedestriansLength;
        void resetAllPedestrians();

        //collision detection
        vec2 *m_tempCarPoints;
        vec2 *m_tempPedPoints;
        vec2 m_tempbboxUpLeft;
        vec2 m_tempbboxDownRight;
        void calculateCollisions();
        void calculateCarCollisions(size_t);
        bool carIsCollidingWithWall(size_t, vec2 &, vec2 &);
        void calculateSensorCollisions();
        std::vector<vec2> m_tempCaptorsIntersections;

        //saving the simulation
        SimulationBank* m_simulationSaves; 
        size_t m_saveSpacing;
        CarState* m_stateHolder;
        double* m_maxFitnessHolder;
        double* m_avgFitnessHolder;
        PedestrianState* m_pedestrianStateHolder;
        void collectStates();


        //going through generations
        float m_deltaTime;
        size_t m_currentGeneration;
        size_t m_stepsPerGeneration;
        GeneticAlgorithm* m_geneticAlgorithm;
        //this resets the car on random location on the starting line, direction of car is parallel to first segment walls
        void reset(); //Tracks  will be regenerated randomly too
        void simulateStep(float dt);

};

#endif