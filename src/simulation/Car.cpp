#include "simulation/Car.h"

Car::Car(vec2 size, 
        vec2 initialPosition, 
        vec2 initialDirection, 
        NNetwork* model,
        size_t captorsRecursionLevel, 
        float captorSparseSeparator) : 
    m_size(size),
    m_position(initialPosition),
    m_direction(initialDirection.fastNormalize()),
    m_speed(0, 0),
    m_lifeTime(0.0f),
    m_points(new vec2[4]),
    m_bboxUpLeft(initialPosition),
    m_bboxDownRight(initialPosition),
    m_isCrashed(false),
    m_won(false),
    m_captors(),
    m_captorsIntersectionPoints(),
    m_captorsRecursionLevel(captorsRecursionLevel),
    m_captorsSparseSeparator(captorSparseSeparator),
    m_model(model),
    m_fitnessRank(0)
{
    updatePoints();
    addCaptors();
}

void Car::addCaptors()
{
    m_captors.clear();
    m_captorsIntersectionPoints.clear();
    vec2 tmpCaptorDirection = m_direction * 1000;
    m_captors.push_back(tmpCaptorDirection);
    m_captorsIntersectionPoints.push_back(m_position + tmpCaptorDirection);

    vec2 check = m_captors[0];
    //1 the right vector
    tmpCaptorDirection = m_captors[0].rotate(1.5707963);
    m_captors.push_back(tmpCaptorDirection);
    m_captorsIntersectionPoints.push_back(m_position + tmpCaptorDirection);

    //2 the left vector
    tmpCaptorDirection = m_captors[0].rotate(-1.5707963);
    m_captors.push_back(tmpCaptorDirection);
    m_captorsIntersectionPoints.push_back(m_position + tmpCaptorDirection);


    //3 the right sparse vector
    tmpCaptorDirection = m_captors[0].rotate(m_captorsSparseSeparator);
    m_captors.push_back(tmpCaptorDirection);
    m_captorsIntersectionPoints.push_back(m_position + tmpCaptorDirection);



    //4 the left sparse vector
    tmpCaptorDirection = m_captors[0].rotate(-m_captorsSparseSeparator);
    m_captors.push_back(tmpCaptorDirection);
    m_captorsIntersectionPoints.push_back(m_position + tmpCaptorDirection);


    //down the recursion hole
    addRecursiveCaptor(0,3, m_captorsRecursionLevel);
    addRecursiveCaptor(0,4, m_captorsRecursionLevel);
    addRecursiveCaptor(1,3, m_captorsRecursionLevel);
    addRecursiveCaptor(2,4, m_captorsRecursionLevel);
}

void Car::addRecursiveCaptor(size_t a,size_t b,size_t recLevel)
{
    if(recLevel == 0) return;
    vec2 newCaptor = (m_captors[a] + m_captors[b]).fastNormalize()*1000;
    m_captors.push_back(newCaptor);
    m_captorsIntersectionPoints.push_back(m_position + newCaptor);
    addRecursiveCaptor(a,m_captors.size()-1,recLevel-1);
    addRecursiveCaptor(m_captors.size()-1,b,recLevel-1);
}


Car::Car(const Car& toCopy) : 
    m_size(toCopy.m_size),
    m_position(toCopy.m_position),
    m_direction(toCopy.m_direction),
    m_speed(toCopy.m_speed),
    m_points(new vec2[4]),
    m_bboxUpLeft(toCopy.m_bboxUpLeft),
    m_bboxDownRight(toCopy.m_bboxDownRight),
    m_isCrashed(toCopy.m_isCrashed),
    m_captors(toCopy.m_captors),
    m_captorsIntersectionPoints(toCopy.m_captorsIntersectionPoints),
    m_captorsRecursionLevel(toCopy.m_captorsRecursionLevel),
    m_captorsSparseSeparator(toCopy.m_captorsSparseSeparator),
    m_model(new NNetwork(*(toCopy.m_model))),
    m_fitnessRank(toCopy.m_fitnessRank)
{
    for (size_t i = 0; i < 4; i++)
    {
        m_points[i] = toCopy.m_points[i];
    }
}

Car& Car::operator=(const Car& toCopy)
{
    if(m_points == nullptr) m_points = new vec2[4];
    m_size = toCopy.m_size;
    m_position = toCopy.m_position;
    m_direction = toCopy.m_direction;
    m_speed = toCopy.m_speed;
    m_bboxUpLeft = toCopy.m_bboxUpLeft;
    m_bboxDownRight = toCopy.m_bboxDownRight;
    m_isCrashed = toCopy.m_isCrashed;
    m_model = new NNetwork(*(toCopy.m_model));
    m_fitnessRank = toCopy.m_fitnessRank;
    for (size_t i = 0; i < 4; i++)
    {
        m_points[i] = toCopy.m_points[i];
    }
    m_captors = toCopy.m_captors;
    m_captorsIntersectionPoints = toCopy.m_captorsIntersectionPoints;
    m_captorsRecursionLevel = toCopy.m_captorsRecursionLevel;
    m_captorsSparseSeparator = toCopy.m_captorsSparseSeparator;
    return *this;
}


Car::~Car()
{
    delete[] m_points;
}

void Car::resetToPosition(vec2 initialPosition, vec2 initialDirection)
{
    m_isCrashed = false;
    m_won = false;
    m_position = initialPosition;
    m_direction = initialDirection.fastNormalize();

    addCaptors();

    m_speed.x = 0;
    m_speed.y = 0;
    m_lifeTime = 0.0f;
    updatePoints();
}

void Car::getPoints(vec2 *points)
{
    for (size_t i = 0; i < 4; i++)
    {
        points[i] = m_points[i];
    }
}

void Car::getBbox(vec2 &bboxUpLeft, vec2 &bboxDownRight)
{
    bboxUpLeft = m_bboxUpLeft;
    bboxDownRight = m_bboxDownRight;
}

vec2 Car::getPosition()
{
    return m_position;
}

std::vector<vec2>& Car::getCaptors()
{
    return m_captors;
}

void Car::setIntersections(std::vector<vec2>& toSet)
{
    for (size_t i = 0; i < toSet.size(); i++)
    {
        m_captorsIntersectionPoints[i] = toSet[i];
    }
}

const std::vector<vec2>& Car::getIntersections( )
{
    return m_captorsIntersectionPoints;
}

const NNetwork* Car::getModel()
{
    return m_model;
}

void Car::setGeneticCode(double* code)
{
    m_model->setGeneticCode(code);
}

void Car::getGeneticCode(double* code)
{
    m_model->getGeneticCode(code);
}

size_t Car::getGeneticCodeSize()
{
    return m_model->getGeneticCodeSize();
}


void Car::simulationStep(float dt)
{
    if (m_isCrashed)
        return;
    // update lifetime
    m_lifeTime += dt;
    // set input layer
    std::vector<double> inputValues(m_captorsIntersectionPoints.size() + 1);
    size_t i = 0;
    for (i = 0; i < m_captorsIntersectionPoints.size(); i++)
    {
        inputValues[i] = 1 / ((((m_captorsIntersectionPoints[i] - m_position).sqrLength())*0.05) + 1);
    }
    inputValues[i] = getSpeed();
    
    std::vector<double> predictionValues;

    // Forward propagation into the NN model
    m_model->propagateForward(inputValues);
    // Get prediction (Forward propagation Outputs)
    m_model->getPrediction(predictionValues);
    

    auto dAngle = predictionValues[0];
    auto dSpeed = predictionValues[1];

    //rotating first
    m_direction = m_direction.rotate(dt * dAngle);
    for (size_t i = 0; i < m_captors.size(); i++)
    {
        m_captors[i] = m_captors[i].rotate(dt*dAngle);
    }
    
    m_speed = m_speed.rotate(dt * dAngle);
    //acceleration
    m_speed = m_speed + dt * dSpeed * m_direction;
    //speed
    m_position = m_position + dt * m_speed;
    updatePoints();
}

void Car::crash()
{
    m_isCrashed = true;
}

bool Car::hasCrashed()
{
    return m_isCrashed;
}

void Car::win()
{
    m_won = true;
}

bool Car::hasWon()
{
    return m_won;
}

double Car::getSpeed()
{
    return sqrt(m_speed.sqrLength());
}

void Car::setLifeTime(float lifeTime)
{
    m_lifeTime = lifeTime;
}

float Car::getLifeTime()
{
    return m_lifeTime;
}

void Car::setState(const CarState& state)
{
    m_position = state.position;
    m_direction = state.direction;
    m_speed = state.speed;
    m_isCrashed = state.crashed;
    m_captorsIntersectionPoints = state.captorCollisionPoints;
    updatePoints();
}

void Car::getState( CarState& state)
{
    state.position = m_position;
    state.direction = m_direction ;
    state.speed = m_speed ;
    state.crashed = m_isCrashed ;
    state.captorCollisionPoints = m_captorsIntersectionPoints;
}

void Car::updatePoints()
{
    vec2 orth = m_direction.fastOrthRight();
    m_points[0] = m_position + (m_size.y * m_direction + m_size.x * orth) * 0.5f;
    m_points[1] = m_position + (-m_size.y * m_direction + m_size.x * orth) * 0.5f;
    m_points[2] = m_position + (-m_size.y * m_direction - m_size.x * orth) * 0.5f;
    m_points[3] = m_position + (m_size.y * m_direction - m_size.x * orth) * 0.5f;

    //getting bounding box
    m_bboxUpLeft.x = m_points[0].x;
    m_bboxUpLeft.y = m_points[0].y;
    m_bboxDownRight.x = m_points[0].x;
    m_bboxDownRight.y = m_points[0].y;
    for (size_t i = 1; i < 4; i++)
    {
        if (m_points[i].x < m_bboxUpLeft.x)
            m_bboxUpLeft.x = m_points[i].x;
        else if (m_points[i].x > m_bboxDownRight.x)
            m_bboxDownRight.x = m_points[i].x;

        if (m_points[i].y > m_bboxUpLeft.y)
            m_bboxUpLeft.y = m_points[i].y;
        else if (m_points[i].y < m_bboxDownRight.y)
            m_bboxDownRight.y = m_points[i].y;
    }
}

size_t Car::getRank()
{
    return m_fitnessRank;
}

void Car::setRank(size_t rank)
{
    m_fitnessRank = rank;
}