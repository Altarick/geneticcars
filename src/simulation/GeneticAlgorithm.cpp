#include "simulation/GeneticAlgorithm.h"

#include <algorithm>
#include <numeric>

GeneticAlgorithm::GeneticAlgorithm(
    std::vector<Car *> cars,
    Tracks *track,
    float mutRate) : m_cars(cars),
                     m_geneticCodes(new double *[m_cars.size()]),
                     m_geneticCodesSize(m_cars[0]->getGeneticCodeSize()),
                     m_genBuffer1(new double[m_geneticCodesSize]),
                     m_genBuffer2(new double[m_geneticCodesSize]),
                     m_genBuffer3(new double[m_geneticCodesSize]),
                     m_fitness(new double[m_cars.size()]),
                     m_tracks(track),
                     m_mutationRate(mutRate),
                     m_currentGeneration(0)
{
    for (size_t i = 0; i < m_cars.size(); i++)
    {
        m_geneticCodes[i] = new double[m_geneticCodesSize];
    }
}

GeneticAlgorithm::~GeneticAlgorithm()
{
    for (size_t i = 0; i < m_cars.size(); i++)
    {
        delete[] m_geneticCodes[i];
    }
    delete[] m_geneticCodes;
    delete[] m_genBuffer3;
    delete[] m_genBuffer2;
    delete[] m_genBuffer1;
    delete[] m_fitness;
}

void GeneticAlgorithm::breedNewGeneration()
{
    //the method to breed is as follow : the best individual will mate with every other (including itself)
    updateFitnessScores();

    // Sort by fitness
    std::vector<size_t> idx(m_cars.size());
    std::iota(idx.begin(), idx.end(), 0);
    std::stable_sort(idx.begin(), idx.end(),
                [&](size_t i1, size_t i2)
                { return m_fitness[i1] > m_fitness[i2]; });

    for (size_t i = 0; i < idx.size(); i++)
    {
        m_cars[idx[i]]->setRank(i + 1);
    }
    
    size_t bestId = idx[0];
    m_cars[bestId]->getGeneticCode(m_genBuffer1);
    for (size_t i = 0; i < m_cars.size(); i++)
    {
        m_cars[i]->getGeneticCode(m_genBuffer2);
        mixGeneticCodes(m_genBuffer1, m_genBuffer2, m_genBuffer3);
        m_cars[i]->setGeneticCode(m_genBuffer3);
    }
    m_currentGeneration += 1;
}

double GeneticAlgorithm::getMaxFitness()
{
    return m_bestFitness;
}

double GeneticAlgorithm::getAverageFitness()
{
    return m_averageFitness;
}


void GeneticAlgorithm::updateFitnessScores()
{
    // fitness here is a negative double (0 minus the square distance to end minus life time)
    m_averageFitness = 0;
    vec2 *lwall;
    vec2 *rwall;
    size_t wallLength;
    wallLength = m_tracks->getWalls(&lwall, &rwall);
    vec2 endPoint = (lwall[wallLength - 1] + rwall[wallLength - 1]) / 2;
    for (size_t i = 0; i < m_cars.size(); i++)
    {
        m_fitness[i] = 0 - ((m_cars[i]->getPosition()) - endPoint).sqrLength();
        m_fitness[i] -= m_cars[i]->getLifeTime()*0.2;
        if(m_cars[i]->hasCrashed())
        {
            if(!(m_cars[i]->hasWon()))m_fitness[i] *= (1 + 0.002*m_currentGeneration);
            else m_fitness[i] /=2;
        }
        m_averageFitness += m_fitness[i] ;
        if(i == 0 || m_fitness[i] > m_bestFitness) m_bestFitness = m_fitness[i];
    }
    m_averageFitness /= m_cars.size();
}

void GeneticAlgorithm::mixGeneticCodes(double *code1, double *code2, double *output)
{
    for (size_t i = 0; i < m_geneticCodesSize; i++)
    {
        if (getRandomTo01() < m_mutationRate) //mutation
        {
            output[i] = getRandomBetween(-1, 1);
        }
        else //no mutation
        {
            // 1 chance out of 2 to choose a codon1 or codon2
            output[i] = getRandomBetween(-1, 1) >= 0 ? code1[i] : code2[i];
        }
    }
}