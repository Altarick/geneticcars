#include "Tracks.h"

#include <simulation/Pedestrian.h>

Tracks::Tracks(size_t nBends,float avgLength,float width,float maxTurn) : 
    m_articulationNumber(nBends),
    m_avgLength(avgLength),
    m_width(width),
    m_maxTurn(maxTurn),
    m_leftWall(new vec2[m_articulationNumber]),
    m_rightWall(new vec2[m_articulationNumber])
{
    regenerate();
}

Tracks::Tracks(const Tracks& toCopy): 
    m_articulationNumber(toCopy.m_articulationNumber),
    m_avgLength(toCopy.m_avgLength),
    m_width(toCopy.m_width),
    m_maxTurn(toCopy.m_maxTurn),
    m_leftWall(new vec2[m_articulationNumber]),
    m_rightWall(new vec2[m_articulationNumber])
{
    for (size_t i = 0; i < m_articulationNumber; i++)
    {
        m_leftWall[i] = toCopy.m_leftWall[i];
        m_rightWall[i] = toCopy.m_rightWall[i];
    }
}

Tracks& Tracks::operator=(const Tracks& toCopy)
{
    if(m_leftWall != nullptr) delete[] m_leftWall;
    if(m_rightWall != nullptr) delete[] m_rightWall;

    m_articulationNumber = toCopy.m_articulationNumber;
    m_avgLength = toCopy.m_avgLength;
    m_width = toCopy.m_width;
    m_maxTurn = toCopy.m_maxTurn;

    m_leftWall = new vec2[m_articulationNumber];
    m_rightWall = new vec2[m_articulationNumber];

    for (size_t i = 0; i < m_articulationNumber; i++)
    {
        m_leftWall[i] = toCopy.m_leftWall[i];
        m_rightWall[i] = toCopy.m_rightWall[i];
    }
    return *this;
}

Tracks::~Tracks()
{
    delete [] m_leftWall;
    delete [] m_rightWall;
}

void Tracks::regenerate()
{
    vec2 p1,p2,p3,direction,avgDirection;

    p2.x = 0;
    p2.y = 0;
    p1.x = getRandomTo01(-0.5f);
    p1.y = getRandomTo01(-0.5f);


    // Generate the track
    for(size_t i = 0; i < m_articulationNumber; i++)
    {
        // Define Track points
        direction = (p2 - p1).normalize();
        p3 = p2 + (direction * m_avgLength * getRandomTo01(0.5f)).rotate(2 * m_maxTurn * getRandomTo01(-0.5f));
        avgDirection = ((direction + (p3 - p2).normalize()) * 0.5f).normalize();
        
        // Build walls around track points (p1, p2, p3..)
        m_rightWall[i] = p2 + (0.5 * m_width * avgDirection.orthRight());
        m_leftWall[i] = p2 - (0.5 * m_width * avgDirection.orthRight());

        // Next step
        p1 = p2;
        p2 = p3;
    }
}

size_t Tracks::getWalls(vec2** leftWall, vec2** rightWall)
{
    *leftWall = new vec2[m_articulationNumber];
    *rightWall = new vec2[m_articulationNumber];

    for (size_t i = 0; i < m_articulationNumber; i++)
    {
        (*leftWall)[i] = m_leftWall[i];
        (*rightWall)[i] = m_rightWall[i];
    }
    return m_articulationNumber;
}

float Tracks::getWidth()
{
    return m_width;
}