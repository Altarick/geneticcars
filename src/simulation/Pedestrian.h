#ifndef PEDESTRIAN_H
#define PEDESTRIAN_H

#include "common.h"
#include "simulation/Tracks.h"

struct PedestrianState
{
    vec2 position;
    vec2 direction;
    vec2 speed;
};

class Pedestrian
{
    public:
        Pedestrian(vec2 size,vec2 initialPosition, vec2 initialDirection, float roadWidth);
        Pedestrian(const Pedestrian& toCopy); 
        ~Pedestrian();

        void set(vec2 initialPosition, vec2 initialDirection);

        //feeds the 4 points into points, hence points must size 4
        void getPoints(vec2* points);
        void getBbox(vec2& bboxUpLeft , vec2& bboxDownRight);
        vec2 getPosition();

        //setter/getter for simulation playback only
        void setState(const PedestrianState& state);
        void getState(PedestrianState& state);

        void simulationStep(float dt,float dSpeed);

        double getSpeed();

        void resetToPosition(vec2 initialPosition, vec2 initialDirection);

    private:
        //the x/y dimensions of the car
        vec2 m_size;
        //current position
        vec2 m_initialPosition;
        vec2 m_position;
        float m_roadWidth;
        //current direction normalized
        vec2 m_direction;
        //current speed
        vec2 m_speed;

        //the points  of the car
        vec2* m_points;
        //the bounding box 
        vec2 m_bboxUpLeft,m_bboxDownRight;

        void tryTurnBack();
        void updatePoints();
};

#endif