#ifndef GENETICALGORITHM_H
#define GENETICALGORITHM_H

#include "simulation/Car.h"
#include "simulation/Tracks.h"

class GeneticAlgorithm
{
public:
    GeneticAlgorithm(std::vector<Car *>, Tracks *track, float mutRate);
    ~GeneticAlgorithm();

    void breedNewGeneration();

    double getMaxFitness();
    double getAverageFitness();

private:
    //the car to use in breeding process
    std::vector<Car *> m_cars;
    //the codes we store in the breeding process
    double **m_geneticCodes;
    size_t m_geneticCodesSize;
    double *m_genBuffer1;
    double *m_genBuffer2;
    double *m_genBuffer3;
    //the fitness
    double *m_fitness;
    //the tracks to calculate fitness
    Tracks *m_tracks;
    float m_mutationRate;

    //the metrics
    double m_bestFitness;
    double m_averageFitness;
    size_t m_currentGeneration;

    void updateFitnessScores();
    void mixGeneticCodes(double *code1, double *code2, double *output);
};

#endif
