#include "simulation/SavedSimulation.h"

#include <neural_network/neuron.h>

SavedSimulation::SavedSimulation(
            Tracks* tracks,
            std::vector<Car*> cars,
            std::vector<Pedestrian*> pedestrians,
            size_t numberOfSteps,
            size_t currentGeneration,
            size_t saveSpacing) :
    m_currentGeneration(currentGeneration),
    m_currentStep(0),
    m_numberOfSteps(numberOfSteps),
    m_cars(),
    m_pedestrians(),
    m_states(new CarState*[m_numberOfSteps]),
    m_tracks(*tracks),
    m_saveSpacing(saveSpacing),
    m_bestFitness(new double[saveSpacing]),
    m_averageFitness(new double[saveSpacing]),
    m_pedestrianStates(new PedestrianState*[m_numberOfSteps])
{
    for (size_t i = 0; i < cars.size(); i++)
    {
        m_cars.push_back(Car(*(cars[i])));
    }
    for (size_t i = 0; i < pedestrians.size(); i++)
    {
        m_pedestrians.push_back(Pedestrian(*(pedestrians[i])));
    }
    for (size_t i = 0; i < m_numberOfSteps; i++)
    {
        m_states[i] = new CarState[m_cars.size()];
    }
    for (size_t i = 0; i < m_numberOfSteps; i++)
    {
        m_pedestrianStates[i] = new PedestrianState[m_pedestrians.size()];
    }
    saveData(".//tmp//gen");
}

SavedSimulation::~SavedSimulation()
{

}

size_t SavedSimulation::getBestCarID()
{
    size_t bestId = 0;
    double bestValue = m_cars[0].getRank();
    for (size_t i = 0; i < m_cars.size(); i++)
    {
        if (m_cars[i].getRank() < bestValue)
        {
            bestValue = m_cars[i].getRank();
            bestId = i;
        }
    }
    return bestId;
}

bool SavedSimulation::saveData(std::string fileName)
{
    size_t bestId = getBestCarID();
    int neuronNum = 0;
    const auto& layers = m_cars[bestId].getModel()->getLayers();
    for (size_t i = 0; i < layers.size(); i++)
    {
        for (size_t j = 0; j < layers[i].size(); j++)
        {
            std::vector<double> vWeights;
            const auto& synapses = layers[i][j]->getSynapses();
            for (size_t k = 0; k < synapses.size(); k++)
            {
                vWeights.push_back(synapses[k].weight);
            }
            setWeightsToFile(vWeights, neuronNum, fileName, m_currentGeneration);
            neuronNum++;
        }
    }
    return true;
}

void SavedSimulation::pushStates(CarState* state, PedestrianState* pdState)
{
    for (size_t i = 0; i < m_cars.size(); i++)
    {
        m_states[m_currentStep][i] = state[i];
    }
    for (size_t i = 0; i < m_pedestrians.size(); i++)
    {
        m_pedestrianStates[m_currentStep][i] = pdState[i];
    }
    m_currentStep += 1;
    if(m_currentStep >= m_numberOfSteps)
    {
        m_currentStep = 0;
    }
}

void SavedSimulation::pushFitnesses(double* avg, double* max)
{
    for (size_t i = 0; i < m_saveSpacing; i++)
    {
        m_bestFitness[i] = max[i];
        m_averageFitness[i] = avg[i];
    }
    
}

void SavedSimulation::step()
{
    m_currentStep = m_currentStep < m_numberOfSteps-1 ? m_currentStep+1 : m_currentStep;
    conformCarsToStep();
    conformPedestriansToStep();
}

void SavedSimulation::stepCycle()
{
    m_currentStep = m_currentStep < m_numberOfSteps-1 ? m_currentStep+1 : 0;
    conformCarsToStep();
    conformPedestriansToStep();
}

void SavedSimulation::stepBack()
{
    m_currentStep = m_currentStep > 0 ? m_currentStep-1 : m_currentStep;
    conformCarsToStep();
    conformPedestriansToStep();
}

void SavedSimulation::stepBackCycle()
{
    m_currentStep = m_currentStep > 0 ? m_currentStep-1 : m_numberOfSteps-1;
    conformCarsToStep();
    conformPedestriansToStep();
}

void SavedSimulation::reach(size_t toReach)
{
    m_currentStep = toReach >= m_numberOfSteps ? m_numberOfSteps -1 : toReach;
    conformCarsToStep();
    conformPedestriansToStep();
}

std::vector<Car*> SavedSimulation::getCars()
{
    std::vector<Car*> ret = std::vector<Car*>(m_cars.size());
    for (size_t i = 0; i < m_cars.size(); i++)
    {
        ret[i] = &(m_cars[i]);
    }
    return ret;
}

std::vector<Pedestrian*> SavedSimulation::getPedestrians()
{
    std::vector<Pedestrian*> ret = std::vector<Pedestrian*>(m_pedestrians.size());
    for (size_t i = 0; i < m_pedestrians.size(); i++)
    {
        ret[i] = &(m_pedestrians[i]);
    }
    return ret;
}

Tracks* SavedSimulation::getTracks()
{
    return &m_tracks;
}

double* SavedSimulation::getMaxFitness()
{
    return m_bestFitness;
}

double* SavedSimulation::getAvgFitness()
{
    return m_averageFitness;
}

void SavedSimulation::conformCarsToStep()
{
    for (size_t i = 0; i < m_cars.size(); i++)
    {
        m_cars[i].setState(m_states[m_currentStep][i]);
    }
}

void SavedSimulation::conformPedestriansToStep()
{
    for (size_t i = 0; i < m_pedestrians.size(); i++)
    {
        m_pedestrians[i].setState(m_pedestrianStates[m_currentStep][i]);
    }
}