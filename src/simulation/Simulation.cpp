#include "simulation/Simulation.h"

Simulation::Simulation( Tracks *tracks, 
                        std::vector<Car *> cars, 
                        std::vector<Pedestrian *> pedestrians, 
                        SimulationBank* simulationStack,
                        size_t saveSpacing,
                        float dt,
                        size_t stepsPerGeneration,
                        GeneticAlgorithm* genAlg) : 
    m_stop(false),
    m_halt(false),
    m_tracks(tracks),
    m_leftWall(nullptr),
    m_rightWall(nullptr),
    m_beginCap(new vec2[4]),
    m_beginWall(new vec2[2]),
    m_endWall(new vec2[2]),
    m_wallArraysLength(0),
    m_cars(cars),
    m_carsLength(cars.size()),
    m_pedestrians(pedestrians),
    m_pedestriansLength(pedestrians.size()),
    m_tempCarPoints(new vec2[4]),
    m_tempPedPoints(new vec2[4]),
    m_simulationSaves(simulationStack),
    m_saveSpacing(saveSpacing),
    m_stateHolder(new CarState[m_carsLength]),
    m_maxFitnessHolder(new double[saveSpacing]),
    m_avgFitnessHolder(new double[saveSpacing]),
    m_pedestrianStateHolder(new PedestrianState[m_pedestriansLength]),
    m_deltaTime(dt),
    m_currentGeneration(0),
    m_stepsPerGeneration(stepsPerGeneration),
    m_geneticAlgorithm(genAlg)
{
    reset();
}

Simulation::~Simulation()
{
    delete[] m_tempPedPoints;
    delete[] m_beginWall;
    delete[] m_endWall;
    delete[] m_beginCap;
    delete[] m_tempCarPoints;
    delete[] m_stateHolder;
    delete[] m_pedestrianStateHolder;
}

void Simulation::simulateGeneration()
{
    m_geneticAlgorithm->breedNewGeneration();
    size_t localGenerationNumber = m_currentGeneration%m_saveSpacing;
    m_maxFitnessHolder[localGenerationNumber] = m_geneticAlgorithm->getMaxFitness();
    m_avgFitnessHolder[localGenerationNumber] = m_geneticAlgorithm->getAverageFitness();
    reset();
    collectStates();
    if(localGenerationNumber == 0)
    {
        SavedSimulation *save = new SavedSimulation(m_tracks, m_cars, m_pedestrians, m_stepsPerGeneration, m_currentGeneration, m_saveSpacing);
        save->pushStates(m_stateHolder, m_pedestrianStateHolder);
        for (size_t i = 1; i < m_stepsPerGeneration; i++)
        {
            simulateStep(m_deltaTime);
            collectStates();
            save->pushStates(m_stateHolder, m_pedestrianStateHolder);
        }
        save->pushFitnesses(m_avgFitnessHolder,m_maxFitnessHolder);
        m_simulationSaves->add(save);
    }
    else
    {
        for (size_t i = 1; i < m_stepsPerGeneration; i++)
        {
            simulateStep(m_deltaTime);
        }
    }
    m_currentGeneration += 1;
}

void Simulation::simulateGenerations(size_t n)
{
    std::cout<< "Starting simulation of generations "<< std::endl;
    for (size_t i = 0; i < n; i++)
    {
        if(i%50 == 0)std::cout<< "Generation " << i << std::endl;
        simulateGeneration();
        {
            std::scoped_lock localLock(m_mutex);
            if (m_stop)
            {
                break;
            }
            
        }
    }
}

void Simulation::simulateGenerationsEndless()
{
    std::cout<< "Starting simulation of generations "<< std::endl;
    size_t i = 0;
    bool haltFlag = false;
    while(true)
    {
        if(i%50 == 0)std::cout<< "Generation " << i << std::endl;
        simulateGeneration();
        {
            std::scoped_lock localLock(m_mutex);
            if (m_stop)
            {
                break;
            }
            if (m_halt)
            {
                std::cout << "Halting simulation ..." << std::endl;
                haltFlag = true;
            }
        }
        while (haltFlag)
        {
            SDL_Delay(250);
            {
                std::scoped_lock localLock(m_mutex);
                if (!m_halt)
                {
                    std::cout << "Resuming simulation ..." << std::endl;
                    haltFlag = false;
                }
                if (m_stop)
                {
                    haltFlag = false;
                    break;
                }
            }
        }
        
        i += 1;
    }
}

void Simulation::stop()
{
    std::scoped_lock localLock(m_mutex);
    m_stop = true;
}

void Simulation::haltSwitch()
{
    std::scoped_lock localLock(m_mutex);
    m_halt = !m_halt;
}

void Simulation::updateTracks()
{
    m_tracks->regenerate();
    m_wallArraysLength = m_tracks->getWalls(&m_leftWall, &m_rightWall);
    m_beginWall[0] = m_leftWall[0];
    m_beginWall[1] = m_rightWall[0];
    m_endWall[0] = m_leftWall[m_wallArraysLength - 1];
    m_endWall[1] = m_rightWall[m_wallArraysLength - 1];
    vec2 widthLeftRight = m_rightWall[0] - m_leftWall[0];
    m_beginCap[0] = m_leftWall[0];
    m_beginCap[1] = m_leftWall[0] + widthLeftRight.rotate(-1.5707963);
    m_beginCap[2] = m_beginCap[1] + widthLeftRight;
    m_beginCap[3] = m_rightWall[0];
}

void Simulation::resetAllCars()
{
    vec2 startDirection = (m_beginWall[0] - m_beginWall[1]).fastOrthRight();
    vec2 startPosition = (m_beginWall[0] + m_beginWall[1]) / 2;
    vec2 startPositionVector = (m_beginWall[0] - startPosition);
    for (size_t i = 0; i < m_carsLength; i++)
    {
        m_cars[i]->resetToPosition(startPosition + (0.5 - g_randomGenerator.randomTo01(g_randomGenerator.random32())) * startPositionVector, startDirection);
    }
}

void Simulation::resetAllPedestrians()
{
    std::vector<int> indexes;
    vec2* leftWall;
    vec2* rightWall;
    size_t nBends = m_tracks->getWalls(&leftWall, &rightWall);
    for (size_t i = 0; i < m_pedestriansLength; i++)
    {
        int direction = getRandomBetween(-1, 1) >= 0 ? 1 : -1;

        int trials = 0;
        int rngIndex = getRandomBetween(1, nBends - 1);
        while(std::find(indexes.begin(), indexes.end(), rngIndex) != indexes.end() && trials++ < 100)
        {
            rngIndex = getRandomBetween(1, nBends - 1);
        }
        indexes.push_back(rngIndex);

        if(direction > 0)
        {
            vec2 initialPos = (leftWall[rngIndex]  + leftWall[rngIndex+1])/2;
            vec2 initialDir = (leftWall[rngIndex + 1] - leftWall[rngIndex]).fastOrthRight();
            m_pedestrians[i]->resetToPosition(initialPos, initialDir);
        }
        else
        {
            vec2 initialPos = (rightWall[rngIndex]  + rightWall[rngIndex+1])/2;
            vec2 initialDir = -1.0 * (rightWall[rngIndex + 1] - rightWall[rngIndex]).fastOrthRight();
            m_pedestrians[i]->resetToPosition(initialPos, initialDir);
        }
    }
}

void Simulation::calculateCollisions()
{
    for (size_t i = 0; i < m_carsLength; i++)
    {
        calculateCarCollisions(i);
    }
}

void Simulation::calculateCarCollisions(size_t carIndex)
{
    m_cars[carIndex]->getBbox(m_tempbboxUpLeft, m_tempbboxDownRight);
    // Pedestrians collisions
    for (size_t i = 0; i < m_pedestriansLength - 1; i++)
    {
        m_pedestrians[i]->getPoints(m_tempPedPoints);
        if (carIsCollidingWithWall(carIndex, m_tempPedPoints[0], m_tempPedPoints[1]))
        {
            m_cars[carIndex]->setLifeTime(m_deltaTime * m_stepsPerGeneration);
            m_cars[carIndex]->crash();
        }
        else if (carIsCollidingWithWall(carIndex, m_tempPedPoints[1], m_tempPedPoints[2]))
        {
            m_cars[carIndex]->setLifeTime(m_deltaTime * m_stepsPerGeneration);
            m_cars[carIndex]->crash();
        }
        else if (carIsCollidingWithWall(carIndex, m_tempPedPoints[2], m_tempPedPoints[3]))
        {
            m_cars[carIndex]->setLifeTime(m_deltaTime * m_stepsPerGeneration);
            m_cars[carIndex]->crash();
        }
        else if (carIsCollidingWithWall(carIndex, m_tempPedPoints[3], m_tempPedPoints[0]))
        {
            m_cars[carIndex]->setLifeTime(m_deltaTime * m_stepsPerGeneration);
            m_cars[carIndex]->crash();
        }
    }
    // Left Wall
    for (size_t i = 0; i < m_wallArraysLength - 1; i++)
    {
        if (!((m_leftWall[i].x > m_tempbboxDownRight.x && m_leftWall[i + 1].x > m_tempbboxDownRight.x) || 
            (m_leftWall[i].x < m_tempbboxUpLeft.x && m_leftWall[i + 1].x < m_tempbboxUpLeft.x) || 
            (m_leftWall[i].y < m_tempbboxDownRight.y && m_leftWall[i + 1].y < m_tempbboxDownRight.y) || 
            (m_leftWall[i].y > m_tempbboxUpLeft.y && m_leftWall[i + 1].y > m_tempbboxUpLeft.y)))
            if (carIsCollidingWithWall(carIndex, m_leftWall[i], m_leftWall[i + 1]))
            {
                m_cars[carIndex]->setLifeTime(m_deltaTime * m_stepsPerGeneration);
                m_cars[carIndex]->crash();
            }
    }
    // Right Wall
    for (size_t i = 0; i < m_wallArraysLength - 1; i++)
    {
        if (!((m_rightWall[i].x > m_tempbboxDownRight.x && m_rightWall[i + 1].x > m_tempbboxDownRight.x) || 
        (m_rightWall[i].x < m_tempbboxUpLeft.x && m_rightWall[i + 1].x < m_tempbboxUpLeft.x) || 
        (m_rightWall[i].y < m_tempbboxDownRight.y && m_rightWall[i + 1].y < m_tempbboxDownRight.y) || 
        (m_rightWall[i].y > m_tempbboxUpLeft.y && m_rightWall[i + 1].y > m_tempbboxUpLeft.y)))
            if (carIsCollidingWithWall(carIndex, m_rightWall[i], m_rightWall[i + 1]))
            {
                m_cars[carIndex]->setLifeTime(m_deltaTime * m_stepsPerGeneration);
                m_cars[carIndex]->crash();
            }
    }
    // Begin Wall
    for (size_t i = 0; i < 3; i++)
    {
        if (!((m_beginCap[i].x > m_tempbboxDownRight.x && m_beginCap[i + 1].x > m_tempbboxDownRight.x) || 
        (m_beginCap[i].x < m_tempbboxUpLeft.x && m_beginCap[i + 1].x < m_tempbboxUpLeft.x) || 
        (m_beginCap[i].y < m_tempbboxDownRight.y && m_beginCap[i + 1].y < m_tempbboxDownRight.y) || 
        (m_beginCap[i].y > m_tempbboxUpLeft.y && m_beginCap[i + 1].y > m_tempbboxUpLeft.y)))
            if (carIsCollidingWithWall(carIndex, m_beginCap[i], m_beginCap[i + 1]))
            {
                m_cars[carIndex]->setLifeTime(m_deltaTime * m_stepsPerGeneration);
                m_cars[carIndex]->crash();
            }
    }
    // End Wall
    if (!((m_rightWall[m_wallArraysLength-1].x > m_tempbboxDownRight.x && m_leftWall[m_wallArraysLength-1].x > m_tempbboxDownRight.x) || 
        (m_rightWall[m_wallArraysLength-1].x < m_tempbboxUpLeft.x && m_leftWall[m_wallArraysLength-1].x < m_tempbboxUpLeft.x) || 
        (m_rightWall[m_wallArraysLength-1].y < m_tempbboxDownRight.y && m_leftWall[m_wallArraysLength-1].y < m_tempbboxDownRight.y) || 
        (m_rightWall[m_wallArraysLength-1].y > m_tempbboxUpLeft.y && m_leftWall[m_wallArraysLength-1].y > m_tempbboxUpLeft.y)))
        if (carIsCollidingWithWall(carIndex, m_rightWall[m_wallArraysLength-1], m_leftWall[m_wallArraysLength-1]))
        {
            m_cars[carIndex]->crash();
            m_cars[carIndex]->win();
        }
}

bool Simulation::carIsCollidingWithWall(size_t carIndex, vec2 &a, vec2 &b)
{
    m_cars[carIndex]->getPoints(m_tempCarPoints);
    if (intersect(m_tempCarPoints[0], m_tempCarPoints[1], a, b))
        return true;
    if (intersect(m_tempCarPoints[1], m_tempCarPoints[2], a, b))
        return true;
    if (intersect(m_tempCarPoints[2], m_tempCarPoints[3], a, b))
        return true;
    return intersect(m_tempCarPoints[3], m_tempCarPoints[0], a, b);
}

void Simulation::calculateSensorCollisions()
{
    std::vector<vec2> tempCaptors;
    float sqrMinDist;
    vec2 tempCollisionPoint;
    vec2 closestCollisionPoint;
    vec2 carPosition;
    for (size_t i = 0; i < m_cars.size(); i++)
    {
        carPosition = m_cars[i]->getPosition();
        tempCaptors = m_cars[i]->getCaptors();
        m_tempCaptorsIntersections.resize(tempCaptors.size());
        for (size_t j = 0; j < tempCaptors.size(); j++)
        {
            sqrMinDist = 100000000;
            for (size_t k = 0; k < m_wallArraysLength-1; k++)
            {
                if(intersectPoint(  carPosition,
                                    carPosition + tempCaptors[j],
                                    m_leftWall[k],m_leftWall[k+1],
                                    tempCollisionPoint)
                    && tempCollisionPoint.sqrLength() < sqrMinDist)
                {
                    closestCollisionPoint = tempCollisionPoint;
                    sqrMinDist = closestCollisionPoint.sqrLength();
                }
                if(intersectPoint(  carPosition,
                                    carPosition + tempCaptors[j],
                                    m_rightWall[k],m_rightWall[k+1],
                                    tempCollisionPoint)
                    && tempCollisionPoint.sqrLength() < sqrMinDist)
                {
                    closestCollisionPoint = tempCollisionPoint;
                    sqrMinDist = closestCollisionPoint.sqrLength();
                }
            }
            //sense cap
            for (size_t k = 0; k < 3; k++)
            {
                if(intersectPoint(  carPosition,
                                    carPosition + tempCaptors[j],
                                    m_beginCap[k],m_beginCap[k+1],
                                    tempCollisionPoint)
                    && tempCollisionPoint.sqrLength() < sqrMinDist)
                {
                    closestCollisionPoint = tempCollisionPoint;
                    sqrMinDist = closestCollisionPoint.sqrLength();
                }
            }
            

            // Sense pedestrians
            for (size_t k = 0; k < m_pedestriansLength; k++)
            {
                m_pedestrians[k]->getPoints(m_tempPedPoints);
                if(intersectPoint(  carPosition,
                                    carPosition + tempCaptors[j],
                                    m_tempPedPoints[0],m_tempPedPoints[1],
                                    tempCollisionPoint)
                    && tempCollisionPoint.sqrLength() < sqrMinDist)
                {
                    closestCollisionPoint = tempCollisionPoint;
                    sqrMinDist = closestCollisionPoint.sqrLength();
                }
                if(intersectPoint(  carPosition,
                                    carPosition + tempCaptors[j],
                                    m_tempPedPoints[1],m_tempPedPoints[2],
                                    tempCollisionPoint)
                    && tempCollisionPoint.sqrLength() < sqrMinDist)
                {
                    closestCollisionPoint = tempCollisionPoint;
                    sqrMinDist = closestCollisionPoint.sqrLength();
                }
                if(intersectPoint(  carPosition,
                                    carPosition + tempCaptors[j],
                                    m_tempPedPoints[2],m_tempPedPoints[3],
                                    tempCollisionPoint)
                    && tempCollisionPoint.sqrLength() < sqrMinDist)
                {
                    closestCollisionPoint = tempCollisionPoint;
                    sqrMinDist = closestCollisionPoint.sqrLength();
                }
                if(intersectPoint(  carPosition,
                                    carPosition + tempCaptors[j],
                                    m_tempPedPoints[3],m_tempPedPoints[0],
                                    tempCollisionPoint)
                    && tempCollisionPoint.sqrLength() < sqrMinDist)
                {
                    closestCollisionPoint = tempCollisionPoint;
                    sqrMinDist = closestCollisionPoint.sqrLength();
                }
            }
            m_tempCaptorsIntersections[j] = closestCollisionPoint;
        }
        m_cars[i]->setIntersections(m_tempCaptorsIntersections);
    }
}

void Simulation::collectStates()
{
    for (size_t i = 0; i < m_carsLength; i++)
    {
        m_cars[i]->getState(m_stateHolder[i]);
    }
    for (size_t i = 0; i < m_pedestriansLength; i++)
    {
        m_pedestrians[i]->getState(m_pedestrianStateHolder[i]);
    }
}

void Simulation::reset()
{
    updateTracks();
    resetAllCars();
    resetAllPedestrians();
}

void Simulation::simulateStep(float dt)
{
    calculateSensorCollisions();
    for (size_t i = 0; i < m_carsLength; i++)
    {
        m_cars[i]->simulationStep(dt);
    }
    for (size_t i = 0; i < m_pedestriansLength; i++)
    {
        m_pedestrians[i]->simulationStep(dt, 0.0f);
    }
    calculateCollisions();
}