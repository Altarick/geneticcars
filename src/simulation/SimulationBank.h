#ifndef SIMULATIONBANK_H
#define SIMULATIONBANK_H

#include "common.h"
#include "simulation/SavedSimulation.h"

class SimulationBank
{
    public:
        SimulationBank();
        ~SimulationBank();

        //get the size of the stack
        size_t size() {std::scoped_lock localLock(m_mutex); return m_saves.size();}

        //add a simulation in a thread safe way
        void add(SavedSimulation*);

        //getter for display
        SavedSimulation* const& operator[](std::size_t i);

    private:
        //the one and only mutex that locks the bank during read/write
        std::mutex m_mutex;

        //the actual vect of simulations
        std::vector<SavedSimulation*> m_saves;
};




#endif