#ifndef SAVEDSIMULATION_H
#define SAVEDSIMULATION_H

#include "common.h"
#include "simulation/Car.h"
#include "simulation/Pedestrian.h"
#include "simulation/Tracks.h"

class SavedSimulation
{
    public:

        SavedSimulation(Tracks*,std::vector<Car*>,std::vector<Pedestrian*>,size_t numberOfSteps, size_t currentgeneration, size_t saveSpacing);
        ~SavedSimulation();

        void pushStates(CarState*, PedestrianState*);
        void pushFitnesses(double* avg, double* max);


        //steps forward, stops at the end
        void step();
        //steps forward, cycles at the end
        void stepCycle(); 
        //steps backward, stops at the beginning
        void stepBack();
        //steps backward, cycles at the beginning
        void stepBackCycle();
        //sets the current step to be toReach, will go to end if out of bound
        void reach(size_t toReach);
        size_t getStep() {return m_currentStep;}
        size_t getMaxStep() {return m_numberOfSteps;}

        size_t getBestCarID();
        bool saveData(std::string filePath);

        std::vector<Car*> getCars();
        std::vector<Pedestrian*> getPedestrians();
        Tracks* getTracks();

        double* getMaxFitness();
        double* getAvgFitness();

    private:
        size_t m_currentGeneration;
        size_t m_currentStep;
        size_t m_numberOfSteps;
        std::vector<Car> m_cars;
        std::vector<Pedestrian> m_pedestrians;
        CarState** m_states;
        Tracks m_tracks;
        size_t m_saveSpacing;
        double* m_bestFitness;
        double* m_averageFitness;
        PedestrianState** m_pedestrianStates;


        void conformCarsToStep();
        void conformPedestriansToStep();
};



#endif