#ifndef CAR_H
#define CAR_H

#include "common.h"
#include "simulation/Tracks.h"
#include <neural_network/neural_network.h>

struct CarState
{
    vec2 position;
    vec2 direction;
    vec2 speed;
    bool crashed;
    std::vector<vec2> captorCollisionPoints;
};


class Car
{
    public:
        Car(vec2 size,vec2 initialPosition, vec2 initialDirection, NNetwork* model,size_t captorsRecusionLevel,float captorSparseSeparatorAngle);
        Car(const Car& toCopy);
        Car& operator=(const Car& toCopy);

        ~Car();

        //the setter to reset the car
        void resetToPosition(vec2 initialPosition, vec2 initialDirection);

        //feeds the 4 points into points, hence points must size 4
        void getPoints(vec2* points);
        void getBbox(vec2& bboxUpLeft , vec2& bboxDownRight);
        vec2 getPosition(); 

        //simulates a time step
        void simulationStep(float dt);

        //this stucks the car
        void crash();
        bool hasCrashed();

        void win();
        bool hasWon();

        // getter for car speed
        double getSpeed();

        // setter/getter for car lifetime
        void setLifeTime(float lifeTime);
        float getLifeTime();

        //setter/getter for simulation playback only
        void setState(const CarState& state);
        void getState( CarState& state);

        //captors management for inputs
        std::vector<vec2>& getCaptors();
        void setIntersections(std::vector<vec2>& toSet);
        const std::vector<vec2>& getIntersections();

        //genetic code and neural network managing
        const NNetwork* getModel();
        void setGeneticCode(double* code);
        void getGeneticCode(double* code);
        size_t getGeneticCodeSize();

        //Rank in simulation
        size_t getRank();
        void setRank(size_t rank);

    private:
        //the x/y dimensions of the car
        vec2 m_size;
        //current position
        vec2 m_position;
        //current direction normalized
        vec2 m_direction;
        //current speed
        vec2 m_speed;
        // life time
        float m_lifeTime;
        //the points  of the car
        vec2* m_points;
        //the bounding box 
        vec2 m_bboxUpLeft,m_bboxDownRight;
        bool m_isCrashed;
        bool m_won;
        //the "antennas" of the car, this is a vect of the coordinates of the extremities of the captors, assuming position is at zero
        std::vector<vec2> m_captors;
        std::vector<vec2> m_captorsIntersectionPoints;
        size_t m_captorsRecursionLevel;
        float m_captorsSparseSeparator;

        // NN model
        NNetwork* m_model;
        // Fitness rank
        size_t m_fitnessRank;

        void updatePoints();
        void addCaptors();
        void addRecursiveCaptor(size_t a,size_t b,size_t recLevel);

};



#endif