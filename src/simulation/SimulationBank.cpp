#include "simulation/SimulationBank.h"

SimulationBank::SimulationBank(/* args */)
{

}

SimulationBank::~SimulationBank()
{
    for (size_t i = 0; i < m_saves.size(); i++)
    {
        delete m_saves[i];
        m_saves[i] = nullptr;
    }
}

void SimulationBank::add(SavedSimulation* toAdd)
{
    std::scoped_lock localLock(m_mutex);
    m_saves.push_back(toAdd);
}

SavedSimulation* const& SimulationBank::operator[](std::size_t i) 
{
    std::scoped_lock localLock(m_mutex);
    return (m_saves[i]);
}