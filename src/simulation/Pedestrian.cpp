#include <simulation/Pedestrian.h>


Pedestrian::Pedestrian(vec2 size, vec2 initialPosition, vec2 initialDirection, float roadWidth) : 
    m_size(size),
    m_initialPosition(initialPosition),
    m_position(initialPosition),
    m_roadWidth(roadWidth),
    m_direction(initialDirection.fastNormalize()),
    m_speed(0.0f, 0.0f),
    m_points(new vec2[4]),
    m_bboxUpLeft(initialPosition),
    m_bboxDownRight(initialPosition)
{
    m_speed = m_direction.normalize();
    updatePoints();
}

Pedestrian::Pedestrian(const Pedestrian& toCopy) : 
    m_size(toCopy.m_size),
    m_initialPosition(toCopy.m_initialPosition),
    m_position(toCopy.m_position),
    m_direction(toCopy.m_direction),
    m_speed(toCopy.m_speed),
    m_points(new vec2[4]),
    m_bboxUpLeft(toCopy.m_bboxUpLeft),
    m_bboxDownRight(toCopy.m_bboxDownRight)
{
    for (size_t i = 0; i < 4; i++)
    {
        m_points[i] = toCopy.m_points[i];
    }
}

Pedestrian::~Pedestrian()
{
    delete[] m_points;
}

void Pedestrian::set(vec2 initialPosition, vec2 initialDirection)
{
    m_position = initialPosition;
    m_direction = initialDirection.fastNormalize();
    m_speed = (m_direction - m_position).fastNormalize() * 0.5f;
    updatePoints();
}

void Pedestrian::getPoints(vec2 *points)
{
    for (size_t i = 0; i < 4; i++)
    {
        points[i] = m_points[i];
    }
}

void Pedestrian::getBbox(vec2 &bboxUpLeft, vec2 &bboxDownRight)
{
    bboxUpLeft = m_bboxUpLeft;
    bboxDownRight = m_bboxDownRight;
}

vec2 Pedestrian::getPosition()
{
    return m_position;
}

void Pedestrian::tryTurnBack()
{
    double iniDist = (m_position - m_initialPosition).sqrLength();
    if(iniDist > 4 * m_roadWidth * m_roadWidth)
    {
        m_direction = -1.0f * m_direction;
        m_speed = m_direction;
        m_initialPosition = m_initialPosition + m_direction * -m_roadWidth;
    }
}

void Pedestrian::simulationStep(float dt, float dSpeed)
{
    //acceleration
    m_speed = m_speed + dt * dSpeed * m_direction;
    //speed
    m_position = m_position + dt * m_speed;
    tryTurnBack();
    updatePoints();
}

double Pedestrian::getSpeed()
{
    return sqrt(m_speed.sqrLength());
}

void Pedestrian::setState(const PedestrianState& state)
{
    m_position = state.position;
    m_direction = state.direction;
    m_speed = state.speed;
    updatePoints();
}

void Pedestrian::getState( PedestrianState& state)
{
    state.position = m_position;
    state.direction = m_direction ;
    state.speed = m_speed ;
}

void Pedestrian::updatePoints()
{
    vec2 orth = m_direction.fastOrthRight();
    m_points[0] = m_position + (m_size.y * m_direction + m_size.x * orth) * 0.5f;
    m_points[1] = m_position + (-m_size.y * m_direction + m_size.x * orth) * 0.5f;
    m_points[2] = m_position + (-m_size.y * m_direction - m_size.x * orth) * 0.5f;
    m_points[3] = m_position + (m_size.y * m_direction - m_size.x * orth) * 0.5f;

    //getting bounding box
    m_bboxUpLeft.x = m_points[0].x;
    m_bboxUpLeft.y = m_points[0].y;
    m_bboxDownRight.x = m_points[0].x;
    m_bboxDownRight.y = m_points[0].y;
    for (size_t i = 1; i < 4; i++)
    {
        if (m_points[i].x < m_bboxUpLeft.x)
            m_bboxUpLeft.x = m_points[i].x;
        else if (m_points[i].x > m_bboxDownRight.x)
            m_bboxDownRight.x = m_points[i].x;

        if (m_points[i].y > m_bboxUpLeft.y)
            m_bboxUpLeft.y = m_points[i].y;
        else if (m_points[i].y < m_bboxDownRight.y)
            m_bboxDownRight.y = m_points[i].y;
    }
}

void Pedestrian::resetToPosition(vec2 initialPosition, vec2 initialDirection)
{
    m_initialPosition = initialPosition;
    m_position = initialPosition;
    m_direction = initialDirection.fastNormalize();
    m_speed = m_direction;
    updatePoints();
}