#ifndef TRACKS_H
#define TRACKS_H

#include "common.h"

class Pedestrian;

class Tracks
{
    public:
        Tracks(size_t nBends,float length,float width,float maxTurn);
        Tracks(const Tracks& toCopy); //copy constructor
        Tracks& operator=(const Tracks& toCopy); //copy operator

        ~Tracks();

        void regenerate();

        //puts walls in leftwall and rightwall and return length of array, you are responsible to free memory afterward
        size_t getWalls(vec2** leftWall, vec2** rightWall);
        float getWidth();

    private:
        //the number of segments in the track +1
        size_t m_articulationNumber;
        //the average length of each segments, real length will be uniform in m_avglenth +-50%
        float m_avgLength;
        //the width of the the track
        float m_width;
        //the maximum turn rate at each bend, in gradian, real turn will be uniform between +-maxturn
        float m_maxTurn;

        //the walls
        vec2* m_leftWall;
        vec2* m_rightWall;
};


#endif