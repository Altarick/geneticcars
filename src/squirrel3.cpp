#include "squirrel3.h"

PRNG::PRNG(uint32_t seed) : m_seed(seed), m_position(0)
{

}

void PRNG::setSeed(uint32_t newseed)
{
    m_seed = newseed;
    m_position = 0;
}

uint32_t PRNG::random32()
{
    //advancing position
    uint32_t value = m_position;
    m_position += 1;

    //mangling the bits
    value *= m_BIT_NOISE1;
    value += m_seed;
    value ^= (value >> 8);
    value += m_BIT_NOISE2;
    value ^= (value << 8);
    value *= m_BIT_NOISE3;
    value ^= (value >> 8);

    return value;
}

uint32_t PRNG::random32(uint32_t toSet)
{
    uint32_t tmp = m_position;
    m_position = toSet;
    uint32_t ret = random32();
    m_position = tmp;
    return ret;
}

void PRNG::setPositionInSeed(uint32_t toSet)
{
    m_position = toSet;
}

float PRNG::randomTo01(uint32_t value)
{
    return ((float) value) / ((float) ( (uint32_t) -1  ) );
}